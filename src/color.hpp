#pragma once

#include <iostream>
#include <memory>
#include <string>

class color;
typedef std::unique_ptr<color> color_ptr;

enum class color_byte_order
{
	RGBA,
	ARGB,
	BGRA,
	ABGR,
};

enum class decoding_hint
{
	integer,
	decimal,
};

class color
{
public:
	color();
	~color();
	explicit color(const float r, const float g, const float b, const float a = 1.0f);
	explicit color(const int r, const int g, const int b, const int a = 255);
	explicit color(const std::string& s);
	//explicit color(const variant& node, DecodingHint hint=DecodingHint::integer);
	//explicit color(const glm::vec4& value);
	//explicit color(const glm::u8vec4& value);
	explicit color(unsigned long n, color_byte_order order = color_byte_order::RGBA);

	float r() const { return color_[0]; }
	float g() const { return color_[1]; }
	float b() const { return color_[2]; }
	float a() const { return color_[3]; }

	float red() const { return color_[0]; }
	float green() const { return color_[1]; }
	float blue() const { return color_[2]; }
	float alpha() const { return color_[3]; }

	int ri() const { return icolor_[0]; }
	int gi() const { return icolor_[1]; }
	int bi() const { return icolor_[2]; }
	int ai() const { return icolor_[3]; }

	int r_int() const { return icolor_[0]; }
	int g_int() const { return icolor_[1]; }
	int b_int() const { return icolor_[2]; }
	int a_int() const { return icolor_[3]; }

	void set_red(int a);
	void set_red(float a);

	void set_green(int a);
	void set_green(float a);

	void set_blue(int a);
	void set_blue(float a);

	void set_alpha(int a);
	void set_alpha(float a);

	uint32_t asARGB() const {
		return (static_cast<uint32_t>(icolor_[3]) << 24)
			| (static_cast<uint32_t>(icolor_[0]) << 16)
			| (static_cast<uint32_t>(icolor_[1]) << 8)
			| (static_cast<uint32_t>(icolor_[2]));
	}

	uint32_t asRGBA() const {
		return (static_cast<uint32_t>(icolor_[0]) << 24)
			| (static_cast<uint32_t>(icolor_[1]) << 16)
			| (static_cast<uint32_t>(icolor_[2]) << 8)
			| (static_cast<uint32_t>(icolor_[3]));
	}

	bool operator==(const color& rhs) const {
		return asRGBA() == rhs.asRGBA();
	}

	std::size_t operator()() const {
		return asRGBA();
	}

	const float* as_float_vec() const {
		return &color_[0];
	}

	//glm::u8vec4 as_u8vec4(color_byte_order order=color_byte_order::RGBA) const {
	//	switch(order) {
	//	case color_byte_order::BGRA: return glm::u8vec4(b_int(), g_int(), r_int(), a_int());
	//	case color_byte_order::ARGB: return glm::u8vec4(a_int(), r_int(), g_int(), b_int());
	//	case color_byte_order::ABGR: return glm::u8vec4(a_int(), b_int(), g_int(), r_int());
	//	default: break;
	//	}
	//	return icolor_;
	//}

	//const float* asFloatVector(color_byte_order order=color_byte_order::RGBA) const {
	//	switch(order) {
	//	case color_byte_order::BGRA: return glm::value_ptr(glm::vec4(color_[2], color_[1], color_[0], color_[3]));
	//	case color_byte_order::ARGB: return glm::value_ptr(glm::vec4(color_[3], color_[0], color_[1], color_[2]));
	//	case color_byte_order::ABGR: return glm::value_ptr(glm::vec4(color_[3], color_[2], color_[1], color_[0]));
	//	default: break;
	//	}
	//	return glm::value_ptr(color_);
	//}

	//glm::u8vec4 to_hsv() const;
	//glm::vec4 to_hsv_vec4() const;
	static color from_hsv(int h, int s, int v, int a=255);
	static color from_hsv(float h, float s, float v, float a=1.0f);

	//variant write() const;

	void pre_multiply();
	void pre_multiply(int alpha);
	void pre_multiply(float alpha);

	static color_ptr factory(const std::string& name);

	static const color& color_aliceblue() { static color res(240, 248, 255); return res; }
	static const color& color_antiquewhite() { static color res(250, 235, 215); return res; }
	static const color& color_aqua() { static color res(0, 255, 255); return res; }
	static const color& color_aquamarine() { static color res(127, 255, 212); return res; }
	static const color& color_azure() { static color res(240, 255, 255); return res; }
	static const color& color_beige() { static color res(245, 245, 220); return res; }
	static const color& color_bisque() { static color res(255, 228, 196); return res; }
	static const color& color_black() { static color res(0, 0, 0); return res; }
	static const color& color_blanchedalmond() { static color res(255, 235, 205); return res; }
	static const color& color_blue() { static color res(0, 0, 255); return res; }
	static const color& color_blueviolet() { static color res(138, 43, 226); return res; }
	static const color& color_brown() { static color res(165, 42, 42); return res; }
	static const color& color_burlywood() { static color res(222, 184, 135); return res; }
	static const color& color_cadetblue() { static color res(95, 158, 160); return res; }
	static const color& color_chartreuse() { static color res(127, 255, 0); return res; }
	static const color& color_chocolate() { static color res(210, 105, 30); return res; }
	static const color& color_coral() { static color res(255, 127, 80); return res; }
	static const color& color_cornflowerblue() { static color res(100, 149, 237); return res; }
	static const color& color_cornsilk() { static color res(255, 248, 220); return res; }
	static const color& color_crimson() { static color res(220, 20, 60); return res; }
	static const color& color_cyan() { static color res(0, 255, 255); return res; }
	static const color& color_darkblue() { static color res(0, 0, 139); return res; }
	static const color& color_darkcyan() { static color res(0, 139, 139); return res; }
	static const color& color_darkgoldenrod() { static color res(184, 134, 11); return res; }
	static const color& color_darkgray() { static color res(169, 169, 169); return res; }
	static const color& color_darkgreen() { static color res(0, 100, 0); return res; }
	static const color& color_darkgrey() { static color res(169, 169, 169); return res; }
	static const color& color_darkkhaki() { static color res(189, 183, 107); return res; }
	static const color& color_darkmagenta() { static color res(139, 0, 139); return res; }
	static const color& color_darkolivegreen() { static color res(85, 107, 47); return res; }
	static const color& color_darkorange() { static color res(255, 140, 0); return res; }
	static const color& color_darkorchid() { static color res(153, 50, 204); return res; }
	static const color& color_darkred() { static color res(139, 0, 0); return res; }
	static const color& color_darksalmon() { static color res(233, 150, 122); return res; }
	static const color& color_darkseagreen() { static color res(143, 188, 143); return res; }
	static const color& color_darkslateblue() { static color res(72, 61, 139); return res; }
	static const color& color_darkslategray() { static color res(47, 79, 79); return res; }
	static const color& color_darkslategrey() { static color res(47, 79, 79); return res; }
	static const color& color_darkturquoise() { static color res(0, 206, 209); return res; }
	static const color& color_darkviolet() { static color res(148, 0, 211); return res; }
	static const color& color_deeppink() { static color res(255, 20, 147); return res; }
	static const color& color_deepskyblue() { static color res(0, 191, 255); return res; }
	static const color& color_dimgray() { static color res(105, 105, 105); return res; }
	static const color& color_dimgrey() { static color res(105, 105, 105); return res; }
	static const color& color_dodgerblue() { static color res(30, 144, 255); return res; }
	static const color& color_firebrick() { static color res(178, 34, 34); return res; }
	static const color& color_floralwhite() { static color res(255, 250, 240); return res; }
	static const color& color_forestgreen() { static color res(34, 139, 34); return res; }
	static const color& color_fuchsia() { static color res(255, 0, 255); return res; }
	static const color& color_gainsboro() { static color res(220, 220, 220); return res; }
	static const color& color_ghostwhite() { static color res(248, 248, 255); return res; }
	static const color& color_gold() { static color res(255, 215, 0); return res; }
	static const color& color_goldenrod() { static color res(218, 165, 32); return res; }
	static const color& color_gray() { static color res(128, 128, 128); return res; }
	static const color& color_grey() { static color res(128, 128, 128); return res; }
	static const color& color_green() { static color res(0, 128, 0); return res; }
	static const color& color_greenyellow() { static color res(173, 255, 47); return res; }
	static const color& color_honeydew() { static color res(240, 255, 240); return res; }
	static const color& color_hotpink() { static color res(255, 105, 180); return res; }
	static const color& color_indianred() { static color res(205, 92, 92); return res; }
	static const color& color_indigo() { static color res(75, 0, 130); return res; }
	static const color& color_ivory() { static color res(255, 255, 240); return res; }
	static const color& color_khaki() { static color res(240, 230, 140); return res; }
	static const color& color_lavender() { static color res(230, 230, 250); return res; }
	static const color& color_lavenderblush() { static color res(255, 240, 245); return res; }
	static const color& color_lawngreen() { static color res(124, 252, 0); return res; }
	static const color& color_lemonchiffon() { static color res(255, 250, 205); return res; }
	static const color& color_lightblue() { static color res(173, 216, 230); return res; }
	static const color& color_lightcoral() { static color res(240, 128, 128); return res; }
	static const color& color_lightcyan() { static color res(224, 255, 255); return res; }
	static const color& color_lightgoldenrodyellow() { static color res(250, 250, 210); return res; }
	static const color& color_lightgray() { static color res(211, 211, 211); return res; }
	static const color& color_lightgreen() { static color res(144, 238, 144); return res; }
	static const color& color_lightgrey() { static color res(211, 211, 211); return res; }
	static const color& color_lightpink() { static color res(255, 182, 193); return res; }
	static const color& color_lightsalmon() { static color res(255, 160, 122); return res; }
	static const color& color_lightseagreen() { static color res(32, 178, 170); return res; }
	static const color& color_lightskyblue() { static color res(135, 206, 250); return res; }
	static const color& color_lightslategray() { static color res(119, 136, 153); return res; }
	static const color& color_lightslategrey() { static color res(119, 136, 153); return res; }
	static const color& color_lightsteelblue() { static color res(176, 196, 222); return res; }
	static const color& color_lightyellow() { static color res(255, 255, 224); return res; }
	static const color& color_lime() { static color res(0, 255, 0); return res; }
	static const color& color_limegreen() { static color res(50, 205, 50); return res; }
	static const color& color_linen() { static color res(250, 240, 230); return res; }
	static const color& color_magenta() { static color res(255, 0, 255); return res; }
	static const color& color_maroon() { static color res(128, 0, 0); return res; }
	static const color& color_mediumaquamarine() { static color res(102, 205, 170); return res; }
	static const color& color_mediumblue() { static color res(0, 0, 205); return res; }
	static const color& color_mediumorchid() { static color res(186, 85, 211); return res; }
	static const color& color_mediumpurple() { static color res(147, 112, 219); return res; }
	static const color& color_mediumseagreen() { static color res(60, 179, 113); return res; }
	static const color& color_mediumslateblue() { static color res(123, 104, 238); return res; }
	static const color& color_mediumspringgreen() { static color res(0, 250, 154); return res; }
	static const color& color_mediumturquoise() { static color res(72, 209, 204); return res; }
	static const color& color_mediumvioletred() { static color res(199, 21, 133); return res; }
	static const color& color_midnightblue() { static color res(25, 25, 112); return res; }
	static const color& color_mintcream() { static color res(245, 255, 250); return res; }
	static const color& color_mistyrose() { static color res(255, 228, 225); return res; }
	static const color& color_moccasin() { static color res(255, 228, 181); return res; }
	static const color& color_navajowhite() { static color res(255, 222, 173); return res; }
	static const color& color_navy() { static color res(0, 0, 128); return res; }
	static const color& color_oldlace() { static color res(253, 245, 230); return res; }
	static const color& color_olive() { static color res(128, 128, 0); return res; }
	static const color& color_olivedrab() { static color res(107, 142, 35); return res; }
	static const color& color_orange() { static color res(255, 165, 0); return res; }
	static const color& color_orangered() { static color res(255, 69, 0); return res; }
	static const color& color_orchid() { static color res(218, 112, 214); return res; }
	static const color& color_palegoldenrod() { static color res(238, 232, 170); return res; }
	static const color& color_palegreen() { static color res(152, 251, 152); return res; }
	static const color& color_paleturquoise() { static color res(175, 238, 238); return res; }
	static const color& color_palevioletred() { static color res(219, 112, 147); return res; }
	static const color& color_papayawhip() { static color res(255, 239, 213); return res; }
	static const color& color_peachpuff() { static color res(255, 218, 185); return res; }
	static const color& color_peru() { static color res(205, 133, 63); return res; }
	static const color& color_pink() { static color res(255, 192, 203); return res; }
	static const color& color_plum() { static color res(221, 160, 221); return res; }
	static const color& color_powderblue() { static color res(176, 224, 230); return res; }
	static const color& color_purple() { static color res(128, 0, 128); return res; }
	static const color& color_red() { static color res(255, 0, 0); return res; }
	static const color& color_rosybrown() { static color res(188, 143, 143); return res; }
	static const color& color_royalblue() { static color res(65, 105, 225); return res; }
	static const color& color_saddlebrown() { static color res(139, 69, 19); return res; }
	static const color& color_salmon() { static color res(250, 128, 114); return res; }
	static const color& color_sandybrown() { static color res(244, 164, 96); return res; }
	static const color& color_seagreen() { static color res(46, 139, 87); return res; }
	static const color& color_seashell() { static color res(255, 245, 238); return res; }
	static const color& color_sienna() { static color res(160, 82, 45); return res; }
	static const color& color_silver() { static color res(192, 192, 192); return res; }
	static const color& color_skyblue() { static color res(135, 206, 235); return res; }
	static const color& color_slateblue() { static color res(106, 90, 205); return res; }
	static const color& color_slategray() { static color res(112, 128, 144); return res; }
	static const color& color_slategrey() { static color res(112, 128, 144); return res; }
	static const color& color_snow() { static color res(255, 250, 250); return res; }
	static const color& color_springgreen() { static color res(0, 255, 127); return res; }
	static const color& color_steelblue() { static color res(70, 130, 180); return res; }
	static const color& color_tan() { static color res(210, 180, 140); return res; }
	static const color& color_teal() { static color res(0, 128, 128); return res; }
	static const color& color_thistle() { static color res(216, 191, 216); return res; }
	static const color& color_tomato() { static color res(255, 99, 71); return res; }
	static const color& color_turquoise() { static color res(64, 224, 208); return res; }
	static const color& color_violet() { static color res(238, 130, 238); return res; }
	static const color& color_wheat() { static color res(245, 222, 179); return res; }
	static const color& color_white() { static color res(255, 255, 255); return res; }
	static const color& color_whitesmoke() { static color res(245, 245, 245); return res; }
	static const color& color_yellow() { static color res(255, 255, 0); return res; }
	static const color& color_yellowgreen() { static color res(154, 205, 50); return res; }		
private:
	void convert_to_icolor();
	void convert_to_color();
	//glm::u8vec4 icolor_;
	//glm::vec4 color_;
	uint8_t icolor_[4];
	float color_[4];
};

std::ostream& operator<<(std::ostream& os, const color& c);

inline bool operator<(const color& lhs, const color& rhs)
{
	return lhs.asARGB() < rhs.asARGB();
}

inline bool operator!=(const color& lhs, const color& rhs)
{
	return !(lhs == rhs);
}

color operator*(const color& lhs, const color& rhs);
