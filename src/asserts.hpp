#pragma once

#include <iostream>
#include <sstream>

#include "spdlog/spdlog.h"

#if defined(_MSC_VER)
#include <intrin.h>
#define DebuggerBreak()		do{ __debugbreak(); } while(0)
#define __SHORT_FORM_OF_FILE__		\
	(strrchr(__FILE__, '\\')		\
	? strrchr(__FILE__, '\\') + 1	\
	: __FILE__						\
	)
#else
#include <signal.h>
#define DebuggerBreak()		do{ raise(SIGINT); }while(0)
#define __SHORT_FORM_OF_FILE__		\
	(strrchr(__FILE__, '/')			\
	? strrchr(__FILE__, '/') + 1	\
	: __FILE__						\
	)
#endif

template <typename... Args>
void log_error(const char* filen, int line_no, const Args & ... args) {
	spdlog::get("console")->error("{}:{} {}\n", filen, line_no, fmt::format(args...));
}

template <typename... Args>
void log_debug(const char* filen, int line_no, const Args & ... args) {
	spdlog::get("console")->debug("{}:{} {}\n", filen, line_no, fmt::format(args...));
}

template <typename... Args>
void log_info(const char* filen, int line_no, const Args & ... args) {
	spdlog::get("console")->info("{}:{} {}\n", filen, line_no, fmt::format(args...));
}

template <typename... Args>
void log_warn(const char* filen, int line_no, const Args & ... args) {
	spdlog::get("console")->warn("{}:{} {}\n", filen, line_no, fmt::format(args...));
}

template <typename... Args>
void assert_log(bool a, const char* filen, int line_no, const Args&... args) {
	if(!(a)) {
		spdlog::get("console")->critical("{}:{} {}\n", filen, line_no, fmt::format(args...));
		DebuggerBreak();
		exit(1);
	}
}

template <typename... Args>
void log_critical(const char* filen, int line_no, const Args & ... args) {
	spdlog::get("console")->critical("{}:{} {}\n",filen, line_no, fmt::format(args...));
	DebuggerBreak();
	exit(1);
}

#define ASSERT_LOG(a, ...)	assert_log(a, __SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
#define LOG_DEBUG(...)		log_debug(__SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
#define LOG_INFO(...)		log_info(__SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
#define LOG_WARN(...)		log_warn(__SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
#define LOG_ERROR(...)		log_error(__SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
#define LOG_CRITICAL(...)	log_critical(__SHORT_FORM_OF_FILE__, __LINE__, __VA_ARGS__);
