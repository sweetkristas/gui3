
#include <algorithm>
#include <map>
#include <limits>
#include <string_view>
#include "asserts.hpp"
#include "color.hpp"
#include "utils.hpp"
#include "string_hash.hpp"

namespace 
{
	template<typename T>
	T clamp(T value, T minval, T maxval)
	{
		return std::min<T>(maxval, std::max(value, minval));
	}

	typedef std::vector<color> color_table_t;
	const color& get_color_from_string(const std::string& s)
	{
		static const color_table_t color_table = {
			color(240, 248, 255),
			color(250, 235, 215),
			color(0, 255, 255),
			color(127, 255, 212),
			color(240, 255, 255),
			color(245, 245, 220),
			color(255, 228, 196),
			color(0, 0, 0),
			color(255, 235, 205),
			color(0, 0, 255),
			color(138, 43, 226),
			color(165, 42, 42),
			color(222, 184, 135),
			color(95, 158, 160),
			color(127, 255, 0),
			color(210, 105, 30),
			color(255, 127, 80),
			color(100, 149, 237),
			color(255, 248, 220),
			color(220, 20, 60),
			color(0, 255, 255),
			color(0, 0, 139),
			color(0, 139, 139),
			color(184, 134, 11),
			color(169, 169, 169),
			color(0, 100, 0),
			color(169, 169, 169),
			color(189, 183, 107),
			color(139, 0, 139),
			color(85, 107, 47),
			color(255, 140, 0),
			color(153, 50, 204),
			color(139, 0, 0),
			color(233, 150, 122),
			color(143, 188, 143),
			color(72, 61, 139),
			color(47, 79, 79),
			color(47, 79, 79),
			color(0, 206, 209),
			color(148, 0, 211),
			color(255, 20, 147),
			color(0, 191, 255),
			color(105, 105, 105),
			color(105, 105, 105),
			color(30, 144, 255),
			color(178, 34, 34),
			color(255, 250, 240),
			color(34, 139, 34),
			color(255, 0, 255),
			color(220, 220, 220),
			color(248, 248, 255),
			color(255, 215, 0),
			color(218, 165, 32),
			color(128, 128, 128),
			color(128, 128, 128),
			color(0, 128, 0),
			color(173, 255, 47),
			color(240, 255, 240),
			color(255, 105, 180),
			color(205, 92, 92),
			color(75, 0, 130),
			color(255, 255, 240),
			color(240, 230, 140),
			color(230, 230, 250),
			color(255, 240, 245),
			color(124, 252, 0),
			color(255, 250, 205),
			color(173, 216, 230),
			color(240, 128, 128),
			color(224, 255, 255),
			color(250, 250, 210),
			color(211, 211, 211),
			color(144, 238, 144),
			color(211, 211, 211),
			color(255, 182, 193),
			color(255, 160, 122),
			color(32, 178, 170),
			color(135, 206, 250),
			color(119, 136, 153),
			color(119, 136, 153),
			color(176, 196, 222),
			color(255, 255, 224),
			color(0, 255, 0),
			color(50, 205, 50),
			color(250, 240, 230),
			color(255, 0, 255),
			color(128, 0, 0),
			color(102, 205, 170),
			color(0, 0, 205),
			color(186, 85, 211),
			color(147, 112, 219),
			color(60, 179, 113),
			color(123, 104, 238),
			color(0, 250, 154),
			color(72, 209, 204),
			color(199, 21, 133),
			color(25, 25, 112),
			color(245, 255, 250),
			color(255, 228, 225),
			color(255, 228, 181),
			color(255, 222, 173),
			color(0, 0, 128),
			color(253, 245, 230),
			color(128, 128, 0),
			color(107, 142, 35),
			color(255, 165, 0),
			color(255, 69, 0),
			color(218, 112, 214),
			color(238, 232, 170),
			color(152, 251, 152),
			color(175, 238, 238),
			color(219, 112, 147),
			color(255, 239, 213),
			color(255, 218, 185),
			color(205, 133, 63),
			color(255, 192, 203),
			color(221, 160, 221),
			color(176, 224, 230),
			color(128, 0, 128),
			color(255, 0, 0),
			color(188, 143, 143),
			color(65, 105, 225),
			color(139, 69, 19),
			color(250, 128, 114),
			color(244, 164, 96),
			color(46, 139, 87),
			color(255, 245, 238),
			color(160, 82, 45),
			color(192, 192, 192),
			color(135, 206, 235),
			color(106, 90, 205),
			color(112, 128, 144),
			color(112, 128, 144),
			color(255, 250, 250),
			color(0, 255, 127),
			color(70, 130, 180),
			color(210, 180, 140),
			color(0, 128, 128),
			color(216, 191, 216),
			color(255, 99, 71),
			color(64, 224, 208),
			color(238, 130, 238),
			color(245, 222, 179),
			color(255, 255, 255),
			color(245, 245, 245),
			color(255, 255, 0),
			color(154, 205, 50),		
		};
		switch(string_hash(s.c_str())) 
		{
#pragma warning(push)
#pragma warning(disable : 4307)
			case string_hash("aliceblue"): return color_table[0]; break;
			case string_hash("antiquewhite"): return color_table[1]; break;
			case string_hash("antique_white"): return color_table[1]; break;
			case string_hash("aqua"): return color_table[2]; break;
			case string_hash("aquamarine"): return color_table[3]; break;
			case string_hash("azure"): return color_table[4]; break;
			case string_hash("beige"): return color_table[5]; break;
			case string_hash("bisque"): return color_table[6]; break;
			case string_hash("black"): return color_table[7]; break;
			case string_hash("blanchedalmond"): return color_table[8]; break;
			case string_hash("blue"): return color_table[9]; break;
			case string_hash("blueviolet"): return color_table[10]; break;
			case string_hash("brown"): return color_table[11]; break;
			case string_hash("burlywood"): return color_table[12]; break;
			case string_hash("cadetblue"): return color_table[13]; break;
			case string_hash("chartreuse"): return color_table[14]; break;
			case string_hash("chocolate"): return color_table[15]; break;
			case string_hash("coral"): return color_table[16]; break;
			case string_hash("cornflowerblue"): return color_table[17]; break;
			case string_hash("cornsilk"): return color_table[18]; break;
			case string_hash("crimson"): return color_table[19]; break;
			case string_hash("cyan"): return color_table[20]; break;
			case string_hash("darkblue"): return color_table[21]; break;
			case string_hash("darkcyan"): return color_table[22]; break;
			case string_hash("darkgoldenrod"): return color_table[23]; break;
			case string_hash("darkgray"): return color_table[24]; break;
			case string_hash("darkgreen"): return color_table[25]; break;
			case string_hash("darkgrey"): return color_table[26]; break;
			case string_hash("darkkhaki"): return color_table[27]; break;
			case string_hash("darkmagenta"): return color_table[28]; break;
			case string_hash("darkolivegreen"): return color_table[29]; break;
			case string_hash("darkorange"): return color_table[30]; break;
			case string_hash("darkorchid"): return color_table[31]; break;
			case string_hash("darkred"): return color_table[32]; break;
			case string_hash("darksalmon"): return color_table[33]; break;
			case string_hash("darkseagreen"): return color_table[34]; break;
			case string_hash("darkslateblue"): return color_table[35]; break;
			case string_hash("darkslategray"): return color_table[36]; break;
			case string_hash("darkslategrey"): return color_table[37]; break;
			case string_hash("darkturquoise"): return color_table[38]; break;
			case string_hash("darkviolet"): return color_table[39]; break;
			case string_hash("deeppink"): return color_table[40]; break;
			case string_hash("deepskyblue"): return color_table[41]; break;
			case string_hash("dimgray"): return color_table[42]; break;
			case string_hash("dimgrey"): return color_table[43]; break;
			case string_hash("dodgerblue"): return color_table[44]; break;
			case string_hash("firebrick"): return color_table[45]; break;
			case string_hash("floralwhite"): return color_table[46]; break;
			case string_hash("forestgreen"): return color_table[47]; break;
			case string_hash("fuchsia"): return color_table[48]; break;
			case string_hash("gainsboro"): return color_table[49]; break;
			case string_hash("ghostwhite"): return color_table[50]; break;
			case string_hash("gold"): return color_table[51]; break;
			case string_hash("goldenrod"): return color_table[52]; break;
			case string_hash("gray"): return color_table[53]; break;
			case string_hash("grey"): return color_table[54]; break;
			case string_hash("green"): return color_table[55]; break;
			case string_hash("greenyellow"): return color_table[56]; break;
			case string_hash("honeydew"): return color_table[57]; break;
			case string_hash("hotpink"): return color_table[58]; break;
			case string_hash("indianred"): return color_table[59]; break;
			case string_hash("indigo"): return color_table[60]; break;
			case string_hash("ivory"): return color_table[61]; break;
			case string_hash("khaki"): return color_table[62]; break;
			case string_hash("lavender"): return color_table[63]; break;
			case string_hash("lavenderblush"): return color_table[64]; break;
			case string_hash("lawngreen"): return color_table[65]; break;
			case string_hash("lemonchiffon"): return color_table[66]; break;
			case string_hash("lightblue"): return color_table[67]; break;
			case string_hash("lightcoral"): return color_table[68]; break;
			case string_hash("lightcyan"): return color_table[69]; break;
			case string_hash("lightgoldenrodyellow"): return color_table[70]; break;
			case string_hash("lightgray"): return color_table[71]; break;
			case string_hash("lightgreen"): return color_table[72]; break;
			case string_hash("lightgrey"): return color_table[73]; break;
			case string_hash("lightpink"): return color_table[74]; break;
			case string_hash("lightsalmon"): return color_table[75]; break;
			case string_hash("lightseagreen"): return color_table[76]; break;
			case string_hash("lightskyblue"): return color_table[77]; break;
			case string_hash("lightslategray"): return color_table[78]; break;
			case string_hash("lightslategrey"): return color_table[79]; break;
			case string_hash("lightsteelblue"): return color_table[80]; break;
			case string_hash("lightyellow"): return color_table[81]; break;
			case string_hash("lime"): return color_table[82]; break;
			case string_hash("limegreen"): return color_table[83]; break;
			case string_hash("linen"): return color_table[84]; break;
			case string_hash("magenta"): return color_table[85]; break;
			case string_hash("maroon"): return color_table[86]; break;
			case string_hash("mediumaquamarine"): return color_table[87]; break;
			case string_hash("mediumblue"): return color_table[88]; break;
			case string_hash("mediumorchid"): return color_table[89]; break;
			case string_hash("mediumpurple"): return color_table[90]; break;
			case string_hash("mediumseagreen"): return color_table[91]; break;
			case string_hash("mediumslateblue"): return color_table[92]; break;
			case string_hash("mediumspringgreen"): return color_table[93]; break;
			case string_hash("mediumturquoise"): return color_table[94]; break;
			case string_hash("mediumvioletred"): return color_table[95]; break;
			case string_hash("midnightblue"): return color_table[96]; break;
			case string_hash("mintcream"): return color_table[97]; break;
			case string_hash("mistyrose"): return color_table[98]; break;
			case string_hash("moccasin"): return color_table[99]; break;
			case string_hash("navajowhite"): return color_table[100]; break;
			case string_hash("navy"): return color_table[101]; break;
			case string_hash("oldlace"): return color_table[102]; break;
			case string_hash("olive"): return color_table[103]; break;
			case string_hash("olivedrab"): return color_table[104]; break;
			case string_hash("orange"): return color_table[105]; break;
			case string_hash("orangered"): return color_table[106]; break;
			case string_hash("orchid"): return color_table[107]; break;
			case string_hash("palegoldenrod"): return color_table[108]; break;
			case string_hash("palegreen"): return color_table[109]; break;
			case string_hash("paleturquoise"): return color_table[110]; break;
			case string_hash("palevioletred"): return color_table[111]; break;
			case string_hash("papayawhip"): return color_table[112]; break;
			case string_hash("peachpuff"): return color_table[113]; break;
			case string_hash("peru"): return color_table[114]; break;
			case string_hash("pink"): return color_table[115]; break;
			case string_hash("plum"): return color_table[116]; break;
			case string_hash("powderblue"): return color_table[117]; break;
			case string_hash("purple"): return color_table[118]; break;
			case string_hash("red"): return color_table[119]; break;
			case string_hash("rosybrown"): return color_table[120]; break;
			case string_hash("royalblue"): return color_table[121]; break;
			case string_hash("saddlebrown"): return color_table[122]; break;
			case string_hash("salmon"): return color_table[123]; break;
			case string_hash("sandybrown"): return color_table[124]; break;
			case string_hash("seagreen"): return color_table[125]; break;
			case string_hash("seashell"): return color_table[126]; break;
			case string_hash("sienna"): return color_table[127]; break;
			case string_hash("silver"): return color_table[128]; break;
			case string_hash("skyblue"): return color_table[129]; break;
			case string_hash("slateblue"): return color_table[130]; break;
			case string_hash("slategray"): return color_table[131]; break;
			case string_hash("slategrey"): return color_table[132]; break;
			case string_hash("snow"): return color_table[133]; break;
			case string_hash("springgreen"): return color_table[134]; break;
			case string_hash("steelblue"): return color_table[135]; break;
			case string_hash("tan"): return color_table[136]; break;
			case string_hash("teal"): return color_table[137]; break;
			case string_hash("thistle"): return color_table[138]; break;
			case string_hash("tomato"): return color_table[139]; break;
			case string_hash("turquoise"): return color_table[140]; break;
			case string_hash("violet"): return color_table[141]; break;
			case string_hash("wheat"): return color_table[142]; break;
			case string_hash("white"): return color_table[143]; break;
			case string_hash("whitesmoke"): return color_table[144]; break;
			case string_hash("yellow"): return color_table[145]; break;
			case string_hash("yellowgreen"): return color_table[146]; break;		
			default:
				throw std::runtime_error("Unable to parse color string.");
		}
		return color_table[string_hash("white")];
#pragma warning(pop)
	}

	//typedef std::map<std::string, color> color_table_type;
	//void create_color_table(color_table_type& color_table)
	//{
	//	color_table["aliceblue"] = color(240, 248, 255);
	//	color_table["antiquewhite"] = color(250, 235, 215);
	//	color_table["antique_white"] = color(250, 235, 215);
	//	color_table["aqua"] = color(0, 255, 255);
	//	color_table["aquamarine"] = color(127, 255, 212);
	//	color_table["azure"] = color(240, 255, 255);
	//	color_table["beige"] = color(245, 245, 220);
	//	color_table["bisque"] = color(255, 228, 196);
	//	color_table["black"] = color(0, 0, 0);
	//	color_table["blanchedalmond"] = color(255, 235, 205);
	//	color_table["blue"] = color(0, 0, 255);
	//	color_table["blueviolet"] = color(138, 43, 226);
	//	color_table["brown"] = color(165, 42, 42);
	//	color_table["burlywood"] = color(222, 184, 135);
	//	color_table["cadetblue"] = color(95, 158, 160);
	//	color_table["chartreuse"] = color(127, 255, 0);
	//	color_table["chocolate"] = color(210, 105, 30);
	//	color_table["coral"] = color(255, 127, 80);
	//	color_table["cornflowerblue"] = color(100, 149, 237);
	//	color_table["cornsilk"] = color(255, 248, 220);
	//	color_table["crimson"] = color(220, 20, 60);
	//	color_table["cyan"] = color(0, 255, 255);
	//	color_table["darkblue"] = color(0, 0, 139);
	//	color_table["darkcyan"] = color(0, 139, 139);
	//	color_table["darkgoldenrod"] = color(184, 134, 11);
	//	color_table["darkgray"] = color(169, 169, 169);
	//	color_table["darkgreen"] = color(0, 100, 0);
	//	color_table["darkgrey"] = color(169, 169, 169);
	//	color_table["darkkhaki"] = color(189, 183, 107);
	//	color_table["darkmagenta"] = color(139, 0, 139);
	//	color_table["darkolivegreen"] = color(85, 107, 47);
	//	color_table["darkorange"] = color(255, 140, 0);
	//	color_table["darkorchid"] = color(153, 50, 204);
	//	color_table["darkred"] = color(139, 0, 0);
	//	color_table["darksalmon"] = color(233, 150, 122);
	//	color_table["darkseagreen"] = color(143, 188, 143);
	//	color_table["darkslateblue"] = color(72, 61, 139);
	//	color_table["darkslategray"] = color(47, 79, 79);
	//	color_table["darkslategrey"] = color(47, 79, 79);
	//	color_table["darkturquoise"] = color(0, 206, 209);
	//	color_table["darkviolet"] = color(148, 0, 211);
	//	color_table["deeppink"] = color(255, 20, 147);
	//	color_table["deepskyblue"] = color(0, 191, 255);
	//	color_table["dimgray"] = color(105, 105, 105);
	//	color_table["dimgrey"] = color(105, 105, 105);
	//	color_table["dodgerblue"] = color(30, 144, 255);
	//	color_table["firebrick"] = color(178, 34, 34);
	//	color_table["floralwhite"] = color(255, 250, 240);
	//	color_table["forestgreen"] = color(34, 139, 34);
	//	color_table["fuchsia"] = color(255, 0, 255);
	//	color_table["gainsboro"] = color(220, 220, 220);
	//	color_table["ghostwhite"] = color(248, 248, 255);
	//	color_table["gold"] = color(255, 215, 0);
	//	color_table["goldenrod"] = color(218, 165, 32);
	//	color_table["gray"] = color(128, 128, 128);
	//	color_table["grey"] = color(128, 128, 128);
	//	color_table["green"] = color(0, 128, 0);
	//	color_table["greenyellow"] = color(173, 255, 47);
	//	color_table["honeydew"] = color(240, 255, 240);
	//	color_table["hotpink"] = color(255, 105, 180);
	//	color_table["indianred"] = color(205, 92, 92);
	//	color_table["indigo"] = color(75, 0, 130);
	//	color_table["ivory"] = color(255, 255, 240);
	//	color_table["khaki"] = color(240, 230, 140);
	//	color_table["lavender"] = color(230, 230, 250);
	//	color_table["lavenderblush"] = color(255, 240, 245);
	//	color_table["lawngreen"] = color(124, 252, 0);
	//	color_table["lemonchiffon"] = color(255, 250, 205);
	//	color_table["lightblue"] = color(173, 216, 230);
	//	color_table["lightcoral"] = color(240, 128, 128);
	//	color_table["lightcyan"] = color(224, 255, 255);
	//	color_table["lightgoldenrodyellow"] = color(250, 250, 210);
	//	color_table["lightgray"] = color(211, 211, 211);
	//	color_table["lightgreen"] = color(144, 238, 144);
	//	color_table["lightgrey"] = color(211, 211, 211);
	//	color_table["lightpink"] = color(255, 182, 193);
	//	color_table["lightsalmon"] = color(255, 160, 122);
	//	color_table["lightseagreen"] = color(32, 178, 170);
	//	color_table["lightskyblue"] = color(135, 206, 250);
	//	color_table["lightslategray"] = color(119, 136, 153);
	//	color_table["lightslategrey"] = color(119, 136, 153);
	//	color_table["lightsteelblue"] = color(176, 196, 222);
	//	color_table["lightyellow"] = color(255, 255, 224);
	//	color_table["lime"] = color(0, 255, 0);
	//	color_table["limegreen"] = color(50, 205, 50);
	//	color_table["linen"] = color(250, 240, 230);
	//	color_table["magenta"] = color(255, 0, 255);
	//	color_table["maroon"] = color(128, 0, 0);
	//	color_table["mediumaquamarine"] = color(102, 205, 170);
	//	color_table["mediumblue"] = color(0, 0, 205);
	//	color_table["mediumorchid"] = color(186, 85, 211);
	//	color_table["mediumpurple"] = color(147, 112, 219);
	//	color_table["mediumseagreen"] = color(60, 179, 113);
	//	color_table["mediumslateblue"] = color(123, 104, 238);
	//	color_table["mediumspringgreen"] = color(0, 250, 154);
	//	color_table["mediumturquoise"] = color(72, 209, 204);
	//	color_table["mediumvioletred"] = color(199, 21, 133);
	//	color_table["midnightblue"] = color(25, 25, 112);
	//	color_table["mintcream"] = color(245, 255, 250);
	//	color_table["mistyrose"] = color(255, 228, 225);
	//	color_table["moccasin"] = color(255, 228, 181);
	//	color_table["navajowhite"] = color(255, 222, 173);
	//	color_table["navy"] = color(0, 0, 128);
	//	color_table["oldlace"] = color(253, 245, 230);
	//	color_table["olive"] = color(128, 128, 0);
	//	color_table["olivedrab"] = color(107, 142, 35);
	//	color_table["orange"] = color(255, 165, 0);
	//	color_table["orangered"] = color(255, 69, 0);
	//	color_table["orchid"] = color(218, 112, 214);
	//	color_table["palegoldenrod"] = color(238, 232, 170);
	//	color_table["palegreen"] = color(152, 251, 152);
	//	color_table["paleturquoise"] = color(175, 238, 238);
	//	color_table["palevioletred"] = color(219, 112, 147);
	//	color_table["papayawhip"] = color(255, 239, 213);
	//	color_table["peachpuff"] = color(255, 218, 185);
	//	color_table["peru"] = color(205, 133, 63);
	//	color_table["pink"] = color(255, 192, 203);
	//	color_table["plum"] = color(221, 160, 221);
	//	color_table["powderblue"] = color(176, 224, 230);
	//	color_table["purple"] = color(128, 0, 128);
	//	color_table["red"] = color(255, 0, 0);
	//	color_table["rosybrown"] = color(188, 143, 143);
	//	color_table["royalblue"] = color(65, 105, 225);
	//	color_table["saddlebrown"] = color(139, 69, 19);
	//	color_table["salmon"] = color(250, 128, 114);
	//	color_table["sandybrown"] = color(244, 164, 96);
	//	color_table["seagreen"] = color(46, 139, 87);
	//	color_table["seashell"] = color(255, 245, 238);
	//	color_table["sienna"] = color(160, 82, 45);
	//	color_table["silver"] = color(192, 192, 192);
	//	color_table["skyblue"] = color(135, 206, 235);
	//	color_table["slateblue"] = color(106, 90, 205);
	//	color_table["slategray"] = color(112, 128, 144);
	//	color_table["slategrey"] = color(112, 128, 144);
	//	color_table["snow"] = color(255, 250, 250);
	//	color_table["springgreen"] = color(0, 255, 127);
	//	color_table["steelblue"] = color(70, 130, 180);
	//	color_table["tan"] = color(210, 180, 140);
	//	color_table["teal"] = color(0, 128, 128);
	//	color_table["thistle"] = color(216, 191, 216);
	//	color_table["tomato"] = color(255, 99, 71);
	//	color_table["turquoise"] = color(64, 224, 208);
	//	color_table["violet"] = color(238, 130, 238);
	//	color_table["wheat"] = color(245, 222, 179);
	//	color_table["white"] = color(255, 255, 255);
	//	color_table["whitesmoke"] = color(245, 245, 245);
	//	color_table["yellow"] = color(255, 255, 0);
	//	color_table["yellowgreen"] = color(154, 205, 50);		
	//}

	//color_table_type& get_color_table() 
	//{
	//	static color_table_type res;
	//	if(res.empty()) {
	//		create_color_table(res);
	//	}
	//	return res;
	//}

	float convert_string_to_number(const std::string& str)
	{
		double value = utils::parse_number(str);

		if(value > 1.0) {
			// Assume it's an integer value.
			return static_cast<float>(value / 255.0);
		} else if(value < 1.0) {
			return static_cast<float>(value);
		} 
		// value = 1.0 -- check the string to try and disambiguate
		if(str == "1" || str.find('.') == std::string::npos) {
			return 1.0f / 255.0f;
		}
		return 1.0f;
	}

	//float convert_numeric(const variant& node, DecodingHint hint)
	//{
	//	if(node.is_float()) {
	//		if(node.as_float() > 1.0 && hint == DecodingHint::INTEGER) {
	//			return clamp<int>(node.as_int32(), 0, 255) / 255.0f;
	//		}
	//		return clamp<float>(node.as_float(), 0.0f, 1.0f);
	//	} else if(node.is_int()) {
	//		if(node.as_float() <= 1.0f && hint == DecodingHint::DECIMAL) {
	//			return clamp<float>(node.as_float(), 0.0f, 1.0f);
	//		}
	//		return clamp<int>(node.as_int32(), 0, 255) / 255.0f;
	//	} else if(node.is_string()) {
	//		return convert_string_to_number(node.as_string());
	//	}
	//	ASSERT_LOG(false, "attribute of color value was expected to be numeric type.");
	//	return 1.0f;
	//}

	bool convert_hex_digit(char d, int* value) 
	{
		if(d >= 'A' && d <= 'F') {
			*value = d - 'A' + 10;
		} else if(d >= 'a' && d <= 'f') {
			*value = d - 'a' + 10;
		} else if(d >= '0' && d <= '9') {
			*value = d - '0';
		} else {
			return false;
		}
		return true;
	}

	bool color_from_hex_string(const std::string& colstr, color* value)
	{
		std::string s = colstr;
		if(s.empty()) {
			return false;
		}
		if(s[0] == '#') {
			s = s.substr(1);
		}
		if(s.length() != 3 && s.length() != 6 && s.length() != 8) {
			return false;
		}
		if(s.length() == 3) {
			int r_hex = 0, g_hex = 0, b_hex = 0;
			if(convert_hex_digit(s[0], &r_hex) && convert_hex_digit(s[1], &g_hex) && convert_hex_digit(s[2], &b_hex)) {
				*value = color((r_hex << 4) | r_hex, (g_hex << 4) | g_hex, (b_hex << 4) | b_hex);
				return true;
			}
			return false;
		}
		int rh_hex = 0, rl_hex = 0, gh_hex = 0, gl_hex = 0, bh_hex = 0, bl_hex = 0;
		if(convert_hex_digit(s[0], &rh_hex) && convert_hex_digit(s[1], &rl_hex) 
			&& convert_hex_digit(s[2], &gh_hex) && convert_hex_digit(s[3], &gl_hex)
			&& convert_hex_digit(s[4], &bh_hex) && convert_hex_digit(s[5], &bl_hex)) {
				int ah_hex = 0xf, al_hex = 0xf;
				if(s.length() == 8) {
					if(!convert_hex_digit(s[6], &ah_hex) || !convert_hex_digit(s[7], &al_hex)) {
						return false;
					}
				}
				*value = color((rh_hex << 4) | rl_hex, (gh_hex << 4) | gl_hex, (bh_hex << 4) | bl_hex, (ah_hex << 4) | al_hex);
				return true;
		}
		return false;
	}

	bool color_from_hsv_string(const std::string& colstr, color* color)
	{
		if(colstr.empty()) {
			return false;
		}
		if(colstr.size() > 5 && colstr.substr(0,4) == "hsv(") {
			float hsv_col[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
			auto buf = utils::split(colstr, ",| |;");
			unsigned n = 0;
			for(auto& s : buf) {
				hsv_col[n] = convert_string_to_number(s);
				if(++n >= 4) {
					break;
				}
			}
			*color = color::from_hsv(hsv_col[0], hsv_col[1], hsv_col[2], hsv_col[3]);
			return true;
		}
		return false;
	}

	struct rgb
	{
		uint8_t r, g, b;
	};

	struct hsv
	{
		uint8_t h, s, v;
	};

	hsv rgb_to_hsv(uint8_t r, uint8_t g, uint8_t b)
	{
		hsv out;
		uint8_t min_color, max_color, delta;

		min_color = std::min(r, std::min(g, b));
		max_color = std::max(r, std::max(g, b));

		delta = max_color - min_color;
		out.v = max_color;
		if(out.v == 0) {
			out.s = 0;
			out.h = 0;
			return out;
		}

		out.s = static_cast<uint8_t>(255.0f * delta / out.v);
		if(out.s == 0) {
			out.h = 0;
			return out;
		}

		if(r == max_color) {
			out.h = static_cast<uint8_t>(43.0f * (g-b)/delta);
		} else if(g == max_color) {
			out.h = 85 + static_cast<uint8_t>(43.0f * (b-r)/delta);
		} else {
			out.h = 171 + static_cast<uint8_t>(43.0f * (r-g)/delta);
		}
		return out;
	}

	rgb hsv_to_rgb(uint8_t h, uint8_t s, uint8_t v)
	{
		rgb out;
			

		if(s == 0) {
			out.r = out.g = out.b = v;
		} else {
			const uint8_t region = h / 43;
			const uint8_t remainder = (h - (region * 43)) * 6; 

			const uint8_t p = (v * (255 - s)) >> 8;
			const uint8_t q = (v * (255 - ((s * remainder) >> 8))) >> 8;
			const uint8_t t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;

			switch(region)
			{
				case 0:  out.r = v; out.g = t; out.b = p; break;
				case 1:  out.r = q; out.g = v; out.b = p; break;
				case 2:  out.r = p; out.g = v; out.b = t; break;
				case 3:  out.r = p; out.g = q; out.b = v; break;
				case 4:  out.r = t; out.g = p; out.b = v; break;
				default: out.r = v; out.g = p; out.b = q; break;
			}
		}
		return out;
	}

	void hsv_to_rgb(const float h, const float s, const float v, float* const out)
	{
		if(std::abs(s) < std::numeric_limits<float>::epsilon()) {
			out[0] = out[1] = out[2] = v;
		} else {
			const float h_dash = h * 360.0f;
			// n.b. we assume h is scaled 0-1 rather than 0-360, hence the scaling factor is 6 rather than 60
			const float region = h_dash / 60.0f;
			const int int_region = static_cast<int>(std::floor(region)) % 6;
			const float remainder = region - std::floor(region);

			const float p = v * (1.0f - s);
			const float q = v * (1.0f - s * remainder);
			const float t = v * (1.0f - s * (1.0f - remainder));

			switch(int_region)
			{
				case 0:  out[0] = v; out[1] = t; out[2] = p; break;
				case 1:  out[0] = q; out[1] = v; out[2] = p; break;
				case 2:  out[0] = p; out[1] = v; out[2] = t; break;
				case 3:  out[0] = p; out[1] = q; out[2] = v; break;
				case 4:  out[0] = t; out[1] = p; out[2] = v; break;
				default: out[0] = v; out[1] = p; out[2] = q; break;
			}
		}
	}

	void rgb_to_hsv(const float* const rgbf, float* const out)
	{
		const float min_color = std::min(rgbf[0], std::min(rgbf[1], rgbf[2]));
		const float max_color = std::max(rgbf[0], std::max(rgbf[1], rgbf[2]));
		const float delta = max_color - min_color;

		out[2] = max_color;
		if(std::abs(out[2]) < std::numeric_limits<float>::epsilon()) {
			out[1] = 0;
			out[0] = 0;
			return;
		}

		out[1] = delta / out[2];
		if(std::abs(out[1]) < std::numeric_limits<float>::epsilon()) {
			out[0] = 0;
			return;
		}

		if(rgbf[0] == max_color) {
			out[0] = (43.0f * (rgbf[1]-rgbf[2])/delta);
		} else if(rgbf[1] == max_color) {
			out[0] = (85.0f/255.0f) + ((43.0f/255.0f) * (rgbf[2]-rgbf[0])/delta);
		} else {
			out[0] = (171.0f/255.0f) + ((43.0f/255.0f) * (rgbf[0]-rgbf[1])/delta);
		}
	}

	bool color_from_basic_string(const std::string& colstr, color* c)
	{
		float value[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
		auto buf = utils::split(colstr, ",| |;");
		if(buf.size() == 0) {
			return false;
		}
		unsigned n = 0;
		for(auto& s : buf) {
			value[n] = convert_string_to_number(s);
			if(++n >= 4) {
				break;
			}
		}
		*c = color(value[0], value[1], value[2], value[3]);
		return true;
	}

	bool color_from_string(const std::string& colstr, color* color)
	{
		ASSERT_LOG(!colstr.empty(), "Empty string passed to color constructor.");
		try {
			*color = get_color_from_string(colstr);
		} catch(std::runtime_error&) {
			if(!color_from_hsv_string(colstr, color)) {
				if(!color_from_hex_string(colstr, color)) {
					if(!color_from_basic_string(colstr, color)) {
						LOG_CRITICAL("Couldn't parse color '{}' from string value.", colstr);
					}
				}
			}
		}
		return true;
	}
}

color::color()
	: icolor_{255, 255, 255, 255}
	, color_{1.0f, 1.0f, 1.0f, 1.0f}
{
}

color::~color()
{
}

color::color(const float r, const float g, const float b, const float a)
	: color_{r, g, b, a}
{
	convert_to_icolor();
}

color::color(const int r, const int g, const int b, const int a)
	: icolor_{
		static_cast<uint8_t>(clamp(r,0,255)), 
		static_cast<uint8_t>(clamp(g,0,255)), 
		static_cast<uint8_t>(clamp(b,0,255)), 
		static_cast<uint8_t>(clamp(a,0,255))}
{
	convert_to_color();
}

//color::color(const glm::vec4& value)
//	: color_(value)
//{
//	convert_to_icolor();
//}
//
//color::color(const glm::u8vec4& value)
//	: icolor_(value)
//{
//	convert_to_color();
//}
//
//color::color(const variant& node, DecodingHint hint)
//	: icolor_(255, 255, 255, 255),
//		color_(1.0f, 1.0f, 1.0f, 1.0f)
//{
//	if(node.is_string()) {
//		color_from_string(node.as_string(), this);
//	} else if(node.is_list()) {
//		ASSERT_LOG(node.num_elements() == 3 || node.num_elements() == 4,
//			"color nodes must be lists of 3 or 4 numbers.");
//		for(size_t n = 0; n != node.num_elements(); ++n) {
//			color_[n] = convert_numeric(node[n], hint);
//			icolor_[n] = static_cast<int>(color_[n] * 255.0f);
//		}
//	} else if(node.is_map()) {
//		if(node.has_key("red")) {
//			color_[0] = convert_numeric(node["red"], hint);
//			icolor_[0] = static_cast<int>(color_[0] * 255.0f);
//		} else if(node.has_key("r")) {
//			color_[0] = convert_numeric(node["r"], hint);
//			icolor_[0] = static_cast<int>(color_[0] * 255.0f);
//		}
//		if(node.has_key("green")) {
//			color_[1] = convert_numeric(node["green"], hint);
//			icolor_[1] = static_cast<int>(color_[1] * 255.0f);
//		} else if(node.has_key("g")) {
//			color_[1] = convert_numeric(node["g"], hint);
//			icolor_[1] = static_cast<int>(color_[1] * 255.0f);
//		}
//		if(node.has_key("blue")) {
//			color_[2] = convert_numeric(node["blue"], hint);
//			icolor_[2] = static_cast<int>(color_[2] * 255.0f);
//		} else if(node.has_key("b")) {
//			color_[2] = convert_numeric(node["b"], hint);
//			icolor_[2] = static_cast<int>(color_[2] * 255.0f);
//		}
//		if(node.has_key("alpha")) {
//			color_[3] = convert_numeric(node["alpha"], hint);
//			icolor_[3] = static_cast<int>(color_[3] * 255.0f);
//		} else if(node.has_key("a")) {
//			color_[3] = convert_numeric(node["a"], hint);
//			icolor_[3] = static_cast<int>(color_[3] * 255.0f);
//		}
//	} else {
//		ASSERT_LOG(false, "Unrecognised color value: " << node.to_debug_string());
//	}
//}

color::color(unsigned long n, color_byte_order order)
{
	int ib0 = (n & 0xff);
	int ib1 = ((n >> 8) & 0xff);
	int ib2 = ((n >> 16) & 0xff);
	int ib3 = ((n >> 24) & 0xff);
	switch(order)
	{
		case color_byte_order::RGBA:
			icolor_[0] = ib3;
			icolor_[1] = ib2;
			icolor_[2] = ib1;
			icolor_[3] = ib0;
			break;
		case color_byte_order::ARGB:
			icolor_[0] = ib2;
			icolor_[1] = ib1;
			icolor_[2] = ib0;
			icolor_[3] = ib3;
			break;
		case color_byte_order::BGRA:
			icolor_[0] = ib1;
			icolor_[1] = ib2;
			icolor_[2] = ib3;
			icolor_[3] = ib0;
			break;
		case color_byte_order::ABGR:
			icolor_[0] = ib0;
			icolor_[1] = ib1;
			icolor_[2] = ib2;
			icolor_[3] = ib3;
			break;
		default: 
			LOG_CRITICAL("Unknown ColorByteOrder value: {}", static_cast<int>(order));
			break;
	}
	convert_to_color();
}

color::color(const std::string& colstr)
{
	ASSERT_LOG(!colstr.empty(), "Empty string passed to color constructor.");
	color_from_string(colstr, this);
}

void color::set_alpha(int a)
{
	icolor_[3] = a;
	color_[3] = clamp<int>(a, 0, 255) / 255.0f;
}

void color::set_alpha(float a)
{
	color_[3] = clamp<float>(a, 0.0f, 1.0f);
	icolor_[3] = static_cast<int>(color_[3] * 255.0f);
}

void color::set_red(int r)
{
	icolor_[0] = r;
	color_[0] = clamp<int>(r, 0, 255) / 255.0f;
}

void color::set_red(float r)
{
	color_[0] = clamp<float>(r, 0.0f, 1.0f);
	icolor_[0] = static_cast<int>(color_[0] * 255.0f);
}

void color::set_green(int g)
{
	icolor_[1] = g;
	color_[1] = clamp<int>(g, 0, 255) / 255.0f;
}

void color::set_green(float g)
{
	color_[1] = clamp<float>(g, 0.0f, 1.0f);
	icolor_[1] = static_cast<int>(color_[1] * 255.0f);
}

void color::set_blue(int b)
{
	icolor_[2] = b;
	color_[2] = clamp<int>(b, 0, 255) / 255.0f;
}

void color::set_blue(float b)
{
	color_[2] = clamp<float>(b, 0.0f, 1.0f);
	icolor_[2] = static_cast<int>(color_[2] * 255.0f);
}

color_ptr color::factory(const std::string& name)
{
	color_ptr p;
	try {
		p = std::make_unique<color>(get_color_from_string(name));
	} catch(std::runtime_error&) {
		LOG_CRITICAL("Couldn't find color '{}' in known color list", name);
	}
	return p;
}

//variant color::write() const
//{
//	// XXX we should store information on how the color value was given to us, if it was
//	// a variant, then output in same format.
//	std::vector<variant> v;
//	v.reserve(4);
//	v.push_back(variant(r()));
//	v.push_back(variant(g()));
//	v.push_back(variant(b()));
//	v.push_back(variant(a()));
//	return variant(&v);
//}

color operator*(const color& lhs, const color& rhs)
{
	return color(lhs.r()*rhs.r(), lhs.g()*rhs.g(), lhs.b()*rhs.b(), lhs.a()*rhs.a());
}

void color::pre_multiply(int alpha)
{
	// ignore current alpha and multiply all the colors by the given alpha, setting the new alpha to fully opaque
	const float a = static_cast<float>(clamp<int>(alpha, 0, 255) / 255.0f);
	color_[0] *= a;
	color_[1] *= a;
	color_[2] *= a;
	color_[3] = 1.0f;
	convert_to_icolor();
}

void color::pre_multiply(float alpha)
{
	// ignore current alpha and multiply all the colors by the given alpha, setting the new alpha to fully opaque
	const float a = clamp<float>(alpha, 0.0f, 1.0f);
	color_[0] *= a;
	color_[1] *= a;
	color_[2] *= a;
	color_[3] = 1.0f;
	convert_to_icolor();
}

void color::pre_multiply()
{
	color_[0] *= color_[3];
	color_[1] *= color_[3];
	color_[2] *= color_[3];
	color_[3] = 1.0f;
}
	
//glm::u8vec4 color::to_hsv() const
//{
//	hsv outp = rgb_to_hsv(ri(), gi(), bi());
//	return glm::u8vec4(outp.h, outp.s, outp.v, ai());
//}
//
//glm::vec4 color::to_hsv_vec4() const
//{
//	glm::vec4 vec;
//	rgb_to_hsv(glm::value_ptr(color_), glm::value_ptr(vec));
//	return vec;
//}

color color::from_hsv(int h, int s, int v, int a)
{
	rgb outp = hsv_to_rgb(h, s, v);
	return color(outp.r, outp.g, outp.b, a);
}

color color::from_hsv(float h, float s, float v, float a)
{
	//glm::vec4 outp;
	//outp.a = a;
	//hsv_to_rgb(h, s, v, glm::value_ptr(outp));
	float out[3];
	hsv_to_rgb(h, s, v, out);
	return color(out[0], out[1], out[2], a);
}

void color::convert_to_icolor()
{
	icolor_[0] = clamp(static_cast<int>(color_[0] * 255.0f), 0, 255);
	icolor_[1] = clamp(static_cast<int>(color_[1] * 255.0f), 0, 255);
	icolor_[2] = clamp(static_cast<int>(color_[2] * 255.0f), 0, 255);
	icolor_[3] = clamp(static_cast<int>(color_[3] * 255.0f), 0, 255);
}

void color::convert_to_color()
{
	color_[0] = clamp(icolor_[0] / 255.0f, 0.0f, 1.0f);
	color_[1] = clamp(icolor_[1] / 255.0f, 0.0f, 1.0f);
	color_[2] = clamp(icolor_[2] / 255.0f, 0.0f, 1.0f);
	color_[3] = clamp(icolor_[3] / 255.0f, 0.0f, 1.0f);
}

std::ostream& operator<<(std::ostream& os, const color& c)
{
	if(c.ai() == 255) {
		os << "rgb(" << c.ri() << "," << c.gi() << "," << c.bi() << ")";
	} else {
		os << "rgba(" << c.ri() << "," << c.gi() << "," << c.bi() << "," << c.ai() << ")";
	}
	return os;
}
