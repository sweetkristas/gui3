#pragma once

#include <vector>
#include <string>

namespace utils
{
	std::vector<std::string> split_ws_comma(const std::string& s, int len_hint=10);
	std::vector<int> split_ws_comma_int(const std::string& s, int len_hint=10);
	std::vector<std::string> split(const std::string& s, std::string_view dropped_delims, std::string_view kept_delims = {});

	double parse_number(const std::string& str);
}
