#include "asserts.hpp"
#include "utils.hpp"

namespace utils
{
	std::vector<std::string> split_ws_comma(const std::string& s, int len_hint)
	{
		//profile::manager pman("split_ws_comma2");
		// profile time ~0.149758 uS
		std::vector<std::string> res;
		res.reserve(len_hint);
		std::string tmp;
		for(auto c : s) {
			if(c == ',' || c == ' ' || c == '\t' || c == '\v') {
				if(!tmp.empty()) {
					res.emplace_back(tmp);
					tmp.clear();
				}
			} else {
				tmp += c;
			}
		}
		if(!tmp.empty()) {
			res.emplace_back(tmp);
		}
		return res;
	}

	std::vector<int> split_ws_comma_int(const std::string& s, int len_hint)
	{
		//profile::manager pman("split_ws_comma_int");
		// profile time ~0.081224 uS for a string of "0 0 512 512"
		std::vector<int> res;
		res.reserve(len_hint);
		int tmp = 0;
		bool tmp_empty = true;
		for(auto c : s) {
			if(c == ',' || c == ' ' || c == '\t' || c == '\v') {
				if(!tmp_empty) {
					res.emplace_back(tmp);
					tmp = 0;
					tmp_empty = true;
				}
			} else if(c == '-') {
				tmp = -tmp;
			} else if(c >= '0' && c <= '9') {
				tmp = tmp * 10 + c - '0';
				tmp_empty = false;
			}
		}
		if(!tmp_empty) {
			res.emplace_back(tmp);
		}
		return res;
	}

	std::vector<std::string> split(const std::string& s, std::string_view dropped_delims, std::string_view kept_delims)
	{
		std::vector<std::string> res;
		res.reserve(10);
		std::string tmp;
		for(auto c : s) {
			if(!dropped_delims.empty() && dropped_delims.find(c) != std::string_view::npos) {
				if(!tmp.empty()) {
					res.emplace_back(tmp);
					tmp.clear();
				}
			} else if(!kept_delims.empty() && kept_delims.find(c) != std::string_view::npos) {
				if(!tmp.empty()) {
					res.emplace_back(tmp);
					tmp.clear();
				}
				res.emplace_back(std::string(1, c));
			} else {
				tmp += c;
			}
		}
		if(!tmp.empty()) {
			res.emplace_back(tmp);
		}
		return res;
	}

	double parse_number(const std::string& str)
	{
		char* end = nullptr;
		double value = strtod(str.c_str(), &end);
		if(value == 0 && str.c_str() == end || value == HUGE_VAL) {
			LOG_CRITICAL("Invalid numeric value: {}", str);
		}
		ASSERT_LOG(errno != ERANGE, "parsed numeric value out-of-range: {}", str);
		return value;
	}
}
