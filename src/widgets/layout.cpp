#include "layout.hpp"
#include "widget.hpp"

#include "../asserts.hpp"

namespace ui_nom
{
    layout::layout(const layout& l)
        : spacing_{ l.spacing_ }
        , children_{}
    {
        for(const auto& child : l.children_) {
            add_child(child->clone());
        }
    }

    widget* layout::add_child(widget_ptr_t&& w)
    {
        widget* ret = w.get();
        w->parent_ = this;
        if(!children_.empty()) {
            w->left_ = children_.back().get();
            children_.back()->right_ = w.get();
        }
        children_.emplace_back(std::move(w));
        return ret;
    }

    void layout::process(double dt, bool changed) 
    {
        if(changed) {
            ASSERT_LOG(parent_ != nullptr, "layout changed but parent is null.");
        }
        handle_process(dt, changed); 
        for(auto& child : children_) {
            child->process(dt, changed);
        }
    }

    void layout::draw(context* ctx) const
    {
        handle_draw(ctx);
        for(auto& child : children_) {
            child->draw(ctx);
        }
    }

    std::pair<int,int> vert_layout::handle_get_min_size() const
    {
        int cumulative_h = 0;
        int max_w = 0;
        for(auto& child : get_children()) {
            auto [child_w, child_h] = child->get_min_size();
            if(child_w > max_w) {
                max_w = child_w;
            }
            cumulative_h += child_h;
        }
        return std::make_pair(max_w, cumulative_h);
    }

    void vert_layout::handle_layout(int w, int h)
    {
        // todo:
    }
 
    std::pair<int,int> horiz_layout::handle_get_min_size() const
    {
        int cumulative_w = 0;
        int max_h = 0;
        for(auto& child : get_children()) {
            auto [child_w, child_h] = child->get_min_size();
            if(child_h > max_h) {
                max_h = child_h;
            }
            cumulative_w += child_w;
        }
        return std::make_pair(cumulative_w, max_h);
    }

   void horiz_layout::handle_layout(int w, int h)
    {
        // ask each child for sizing, based on w/h
        auto [cumulative_w, max_h] = get_min_size();
        LOG_INFO("cumulative width: {}, maximum height: {}", cumulative_w, max_h);

        const int expand_width = w - cumulative_w;
        if(expand_width < 0) {
            LOG_WARN("Not enough width to place all items at minimum size. {} < {}", w, cumulative_w);
        }
        const int expand_height = h - max_h;
        if(expand_height < 0) {
            LOG_WARN("Not enough height to place all items at minimum size. {} < {}", h, max_h);
        }

        if(expand_width > 0) {
            std::vector<widget*> exchildren;
            for(auto& child : get_children()) {
                if(child->policy().get_horiz() & policy::ignore) {
                    exchildren.emplace_back(child.get());
                }
            }
            if(exchildren.empty()) {
                for(auto& child : get_children()) {
                    if(child->policy().get_horiz() & policy::expand) {
                        exchildren.emplace_back(child.get());
                    }
                }
            }
            if(exchildren.empty()) {
                for(auto& child : get_children()) {
                    if(child->policy().get_horiz() & policy::grow) {
                        exchildren.emplace_back(child.get());   
                    }
                }
            }
            if(!exchildren.empty()) {
                const int delta_w = expand_width / exchildren.size();
                for(auto& child : exchildren) {
                    child->expand_width(delta_w);
                }
            }
        } else {
            std::vector<widget*> exchildren;
            for(auto& child : get_children()) {
                if(child->policy().get_horiz() & policy::shrink) {
                    exchildren.emplace_back(child.get());
                }
            }
            if(!exchildren.empty()) {
                const int shrink_width = -expand_width / exchildren.size();
                for(auto& child : exchildren) {
                    child->shrink_width(shrink_width);
                }
            }        
        }
    }

    void horiz_layout::handle_assign_location(int x, int y) const
    {
        for(auto& child : get_children()) {
            child->set_location(x, y);
            x += child->get_width();
            if(auto lo = child->get_layout()) {
                lo->assign_location(x, y);
            }
        }
    }

}
