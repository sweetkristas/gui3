#include "context.hpp"

#include "GL/gl3w.h"
#include "nanovg.h"
#define NANOVG_GL3_IMPLEMENTATION
#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

#include "SDL2/SDL.h"

namespace
{
    int load_fonts(NVGcontext* vg)
    {
        int font;
        font = nvgCreateFont(vg, "sans", "data/fonts/Roboto-Regular.ttf");
        if (font == -1) {
            spdlog::get("console")->error("Could not add font regular.\n");
            return -1;
        }
        font = nvgCreateFont(vg, "sans-bold", "data/fonts/Roboto-Bold.ttf");
        if (font == -1) {
            spdlog::get("console")->error("Could not add font bold.\n");
            return -1;
        }
        return 0;
    }
}

namespace ui_nom
{
    context::context() 
    {
        auto vg = nvgCreateGL3(NVG_STENCIL_STROKES | NVG_DEBUG);
        ASSERT_LOG(vg != nullptr, "Unable to init nanovg.");
        ctx_ = vg;

        int ret = load_fonts(vg);
        ASSERT_LOG(ret != -1, "Failed to load fonts.");

    }   

    context::~context() 
    {
        nvgDeleteGL3(std::any_cast<NVGcontext*>(ctx_));
    }

    context* context::get_context()
    {
        static auto ctx = std::make_unique<context>();
        return ctx.get();
    }

    void context::begin_frame(int w, int h, float ratio)
    {
        nvgBeginFrame(get<NVGcontext*>(), static_cast<float>(w), static_cast<float>(h), ratio);

        auto ms = SDL_GetMouseState(&mx_, &my_);
        mouse_state_ = bitmask<mouse_state>{};
        if(ms & SDL_BUTTON_LMASK) {
            mouse_state_ |= mouse_state::LEFT;
        }
        if(ms & SDL_BUTTON_RMASK) {
            mouse_state_ |= mouse_state::RIGHT;
        }
        if(ms & SDL_BUTTON_MMASK) {
            mouse_state_ |= mouse_state::MIDDLE;
        }
        if(ms & SDL_BUTTON_X1MASK) {
            mouse_state_ |= mouse_state::X1;
        }
        if(ms & SDL_BUTTON_X2MASK) {
            mouse_state_ |= mouse_state::X2;
        }
    }

    void context::end_frame(double dt)
    {
        if(show_frame_time_) {
            auto vg = get<NVGcontext*>();
            auto str = fmt::format("Frame Time: {:03.01f} mS", dt * 1e3);
            float bounds[4];

            nvgFontSize(vg, 24.0f);
            nvgFontFace(vg, "sans");

            nvgTextBounds(vg, 0.0f, 24.0f, str.c_str(), NULL, bounds);
            nvgBeginPath(vg);
            nvgRoundedRect(vg, bounds[0], bounds[1], bounds[2] - bounds[0], bounds[3] - bounds[1], 2.5f);
            nvgFillColor(vg, nvgRGBA(255,255,255,128));
            nvgFill(vg);

            nvgFillColor(vg, nvgRGBA(0,0,0,255));
            nvgText(vg, 0.0f, 24.0f, str.c_str(), nullptr);
        }

        nvgEndFrame(get<NVGcontext*>());
    }
}