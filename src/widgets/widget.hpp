#pragma once

#include <functional>
#include <map>
#include <string_view>

#include "bitmask.hpp"
#include "geometry.hpp"
#include "layout.hpp"
#include "policy.hpp"

namespace ui_nom
{
    enum class MBP 
    {
        left,
        top,
        right,
        bottom,
    };

    class widget
    {
    public:
        widget() = default;
        virtual ~widget() {}
        widget(const widget& w)
            : hit_area_{ w.hit_area_ }
            , focused_{ w.focused_ }
            , hovered_{ w.hovered_ }
            , enabled_{ w.enabled_ }
            , policy_{ w.policy_ }
            , id_{ w.id_ }
            , class_{ w.class_ }
            , layout_{ w.layout_ != nullptr ? w.layout_->clone() : nullptr }
            , parent_{}
            , left_{}
            , right_{}
            , content_width_{ w.content_width_ }
            , content_height_{ w. content_height_ }
            , x_{ w.x_ }
            , y_{ w.y_ }
            , fixed_width_{ w.fixed_width_ }
            , fixed_height_{ w.fixed_height_ }
        {
        }
        widget& operator=(const widget& w) = default;
        widget(widget&&) = default;
        widget& operator=(widget&&) = default;

        //int width() const { return width_; }
        //int height() const { return height_; }
        
        void set_policy(const sizing_policy& p) { policy_ = p; }
        void set_policy(sizing_policy&& p) { policy_ = p; }
        const sizing_policy& policy() const { return policy_; }

        bool is_mouse_over() const { return hovered_; }
        bool is_focused() const { return focused_; }

        void set_layout(layout_ptr_t&& layout) {             
            layout_ = std::move(layout);
            layout_->set_parent(this);
        }
        const layout* get_layout() const { return layout_.get(); }

        template<typename A, typename... T>
        static std::unique_ptr<A> create(T&&...all) {
            return std::make_unique<A>(std::forward<T>(all)...);
        }

        void process(double dt, int width, int height) { 
            bool changed = false;
            if(width != fixed_width_
             || height != fixed_height_) {
                set_fixed_size(width, height);
                changed = true;
                if(layout_) {
                    // xxx layout us too
                    layout_->do_layout(width, height);
                    layout_->assign_location(0, 0);
                }
                // XXX recompute layout
            }
            handle_process(dt, changed);
            if(layout_) {
                layout_->process(dt, changed);
            }
        }

        void draw(context* ctx) const {
            handle_draw(ctx);
            if(layout_) {
                layout_->draw(ctx);
            }
        }

        bool enable(bool en = true) { std::swap(en, enabled_); return en; }

        // deep copy.
        widget_ptr_t clone() const { return handle_clone(); }

        int get_margins(MBP t) const { return margins_[static_cast<int>(t)]; }
        int get_border(MBP t) const { return border_[static_cast<int>(t)]; }
        int get_padding(MBP t) const { return padding_[static_cast<int>(t)]; }
        int get_mbp(MBP t) const { return get_margins(t) + get_border(t) + get_padding(t); }

        int get_mbp_width() const { return get_mbp(MBP::left) + get_mbp(MBP::right); }
        int get_mbp_height() const { return get_mbp(MBP::top) + get_mbp(MBP::bottom); }

        // retuns min-width, min-heightt
        std::pair<int,int> get_min_size() const { 
            if(layout_ != nullptr) {
                return layout_->get_min_size();
            } else {
                return std::make_pair(content_width_ + get_mbp_width(),
                    content_height_ + get_mbp_height());
            }
        }

        void expand_width(int w) { expand_width_ = w; }
        void expand_height(int h) { expand_height_ = h; }
        void shrink_width(int w) { expand_width_ = -w; }
        void shrink_height(int h) { expand_height_ = -h; }

        int get_width() const { return content_width_ + expand_width_ + get_mbp_width(); }
        int get_height() const { return content_height_ + expand_height_ + get_mbp_height(); }

        void set_fixed_size(int w, int h) { 
            fixed_width_ = w;
            fixed_height_ = h;
            content_width_ = fixed_width_ - get_mbp_width();
            content_height_ = fixed_height_ - get_mbp_height();
            expand_width_ = expand_height_ = 0;
        }
        void set_location(int x, int y) { x_ = x; y_ = y; }
        int get_x() const { return x_; }
        int get_y() const { return y_; }

        friend class layout;
    protected:
        // process version accessible from friends and derived classes.
        void process(double dt, bool changed) { 
            handle_process(dt, changed);
            if(layout_) {
                layout_->process(dt, changed);
            }
        }
        void set_content_size(int w, int h) {
            content_width_ = w;
            content_height_ = h;
        }
        std::pair<int,int> get_content_size() const { 
            return std::make_pair(content_width_ + expand_width_, content_height_ + expand_height_); 
        }
    private:
        virtual void handle_process(double dt, bool changed) {}
        virtual void handle_draw(context* ctx) const {
            /*
            if(texture != nullptr) {
                nvgBeginPath(vg);

                imgPaint = nvgImagePattern(vg, x, y, w, h, 0, img, 1.0f);

                nvgRect(vg, x, y, w, h);
                nvgFillPaint(vg, imgPaint);
                nvgFill(vg);                
            }
            */
        }
        virtual widget_ptr_t handle_clone() const = 0;
        virtual bool on_mouse_over(bitmask<key_modifiers> mods) { return false; }
        virtual bool on_mouse_pressed(bitmask<key_modifiers> mods) { return false; }
        virtual bool on_key_press(bitmask<key_modifiers> mods) { return false; }
        // XXX widget properties.
        rect_t hit_area_{};
        bool focused_ = false;
        bool hovered_ = false;
        bool enabled_ = true;
        sizing_policy policy_{};
        std::string id_{};
        std::string class_{};
        layout_ptr_t layout_{};

        layout* parent_ = nullptr;
        widget* left_ = nullptr;
        widget* right_ = nullptr;

        int content_width_ = 0;
        int content_height_ = 0;

        // amount the widget has been grown/expanded or shrunk(negative)
        // compared to the actual content_width.
        int expand_width_ = 0;
        int expand_height_ = 0;

        //////////////////////////////////////////////////
        //
        //    +------------------------------------+
        //    |               Margins              |
        //    |   +----------------------------+   |
        //    |   |           Border           |   |       
        //    |   |     +------------------+   |   | 
        //    |   |     |     Padding      |   |   |
        //    |   |     |   +---------+    |   |   |
        //    |   |     |   | Content |    |   |   |
        //    |   |     |   |         |    |   |   |
        //    |   |     |   |         |    |   |   |
        //    |   |     |   +---------+    |   |   |
        //    |   |     +------------------+   |   |
        //    |   |                            |   |
        //    |   +----------------------------+   |
        //    |                                    |
        //    +------------------------------------+
        //
        //////////////////////////////////////////////////
        std::array<int, 4> margins_{};
        std::array<int, 4> border_{};
        std::array<int, 4> padding_{};

        int x_ = 0;
        int y_ = 0;

        int fixed_width_ = 0;
        int fixed_height_ = 0;
    };

    class dialog : public widget
    {
    public:
        void show(bool en);
    private:
        widget_ptr_t handle_clone() const override { 
            return std::make_unique<dialog>(*this);
        }
    };

    // class windows : public widget
    // {

    // };

    // class menu : public widget
    // {

    // };
}
