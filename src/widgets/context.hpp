#pragma once

#include <any>

#include "ui_nom_fwd.hpp"
#include "bitmask.hpp"

#include "../asserts.hpp"

namespace ui_nom
{
    class context
    {
    public:
        context();
        ~context();
        void begin_frame(int w, int h, float ratio);
        void end_frame(double dt);

        static context* get_context();

        template<typename T>
        T get() const { return std::any_cast<T>(ctx_); }

        std::pair<int,int> get_mouse_pos() const { return std::make_pair(mx_, my_); }
        int get_mouse_x() const { return mx_; }
        int get_mouse_y() const { return my_; }
        bitmask<mouse_state> get_mouse_state() const { return mouse_state_; }
        void show_frame_time(bool ft = true) { show_frame_time_ = ft; }
    private:
        std::any ctx_{};
        bitmask<mouse_state> mouse_state_{};
        int mx_ = 0;
        int my_ = 0;
        bool show_frame_time_ = true;
    };
}
