#pragma once

#include "ui_nom_fwd.hpp"

namespace ui_nom
{
    // Base layout class
    class layout
    {
    public:
        layout() = default;
        virtual ~layout() {}
        layout(layout&&) = default;
        layout& operator=(layout&&) = default;
        layout(const layout& l);
        layout& operator=(const layout& l) = default;

        int set_spacing(int spacing) { std::swap(spacing_, spacing); return spacing; }
        void set_parent(widget* w) { parent_ = w; }

        void set_alignment(bitmask<alignment> a) { align_ = a; }
        const bitmask<alignment> get_alignment() const { return align_; }
        
        template<typename A, typename... T>
        static std::unique_ptr<A> create(T&&...all) {
            return std::make_unique<A>(std::forward<T>(all)...);
        }

        widget* add_child(widget_ptr_t&& w);

        template<typename T>
        T* add_widget(widget_ptr_t&& w) {
            return static_cast<T*>(add_child(std::move(w)));
        }

        template<typename A, typename... T>
        A* add_widget(T&&...all) {
            return static_cast<A*>(add_child(
                std::make_unique<A>(std::forward<T>(all)...)
            ));
        }
        layout_ptr_t clone() const { return handle_clone(); }
         void process(double dt, bool changed);
         void draw(context* ctx) const;
         void do_layout(int w, int h) {
            handle_layout(w, h);
         }

         void assign_location(int x, int y) const { return handle_assign_location(x, y); }

        std::pair<int,int> get_min_size() const { return handle_get_min_size(); }
    protected:
        widget* get_parent() const { return parent_; }
        const std::vector<widget_ptr_t>& get_children() const { return children_; }
    private:
        virtual layout_ptr_t handle_clone() const = 0;
        virtual void handle_process(double dt, bool changed) {}
        virtual void handle_draw(context* ctx) const {}
        virtual void handle_layout(int w, int h) {}
        virtual void handle_assign_location(int x, int y) const {}
        virtual std::pair<int,int> handle_get_min_size() const = 0;
        int spacing_ = 0;
        std::vector<widget_ptr_t> children_{};
        widget* parent_ = nullptr;
        bitmask<alignment> align_ = alignment::CENTER;
    };

    // Things are laid out horizontally
    class horiz_layout : public layout
    {
    public:
        horiz_layout() = default;
    private:
        layout_ptr_t handle_clone() const override
        {
            return std::make_unique<horiz_layout>(*this);
        }
        void handle_layout(int w, int h) override;
        void handle_assign_location(int x, int y) const override;
        std::pair<int,int> handle_get_min_size() const override;
    };

    // Things are laid out vertically
    class vert_layout : public layout
    {
    public:
    private:
        layout_ptr_t handle_clone() const override
        {
            return std::make_unique<vert_layout>(*this);
        }
        void handle_layout(int w, int h) override;
        std::pair<int,int> handle_get_min_size() const override;
    };

    // Items are stacked on top of each other, such that only one 
    // thing is visible at a time.
    class stack_layout : public layout
    {
    public:
    private:
        layout_ptr_t handle_clone() const override
        {
            return std::make_unique<stack_layout>(*this);
        }
        std::pair<int,int> handle_get_min_size() const override {
            return std::make_pair(0, 0);
        }
    };

    // Items are laid out in a grid
    class grid_layout : public layout
    {
    public:
    private:
        layout_ptr_t handle_clone() const override
        {
            return std::make_unique<grid_layout>(*this);
        }
        std::pair<int,int> handle_get_min_size() const override {
            return std::make_pair(0, 0);
        }
    };

    // specialisation of a grid layout that lays item out in a two column grid,
    // as commonly used for properties type displays.
    class field_layout : public layout
    {
    public:
    private:
        layout_ptr_t handle_clone() const override
        {
            return std::make_unique<field_layout>(*this);
        }
        std::pair<int,int> handle_get_min_size() const override {
            return std::make_pair(0, 0);
        }
    };

}