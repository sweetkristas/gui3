#pragma once

#include "bitmask.hpp"

namespace ui_nom
{
    enum class policy
    {
        fixed       = 0,
        grow        = (1 << 0),
        expand      = (1 << 1), 
        shrink      = (1 << 2),
        ignore      = (1 << 3),

        minimum     = grow,
        maximum     = shrink,
        preferred   = grow | shrink,
        expanding   = grow | shrink | expand,
        minimum_expanding = grow | expand,
    };
    
    class sizing_policy
    {
    public:
        sizing_policy() = default;
        explicit sizing_policy(bitmask<policy> ph, bitmask<policy> pv) : horz_p_{ph}, vert_p_{pv} {}
        void set_horizital_policy(bitmask<policy> new_h) { horz_p_ = new_h; }
        void set_vertical_policy(bitmask<policy> new_v) { vert_p_ = new_v; }
        std::pair<bitmask<policy>, bitmask<policy>> get() const { return std::make_pair(horz_p_, vert_p_); }
        bitmask<policy> get_horiz() const { return horz_p_; }
        bitmask<policy> get_vert() const { return vert_p_; }
    private:
        bitmask<policy> horz_p_ = policy::preferred;
        bitmask<policy> vert_p_ = policy::preferred;
    };
}
