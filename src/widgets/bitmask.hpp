#pragma once

// based on code from https://gpfault.net/posts/typesafe-bitmasks.txt.html
#include <type_traits>

template <class option_type,
          // The line below ensures that bitmask can only be used with enums.
          typename = typename std::enable_if<std::is_enum<option_type>::value>::type>
class bitmask 
{
    // The type we'll use for storing the value of our bitmask should be the same
    // as the enum's underlying type.
    using underlying_type = typename std::underlying_type<option_type>::type;

    static constexpr underlying_type mask_value(option_type o) {
        return static_cast<underlying_type>(o);
    }

    // Private ctor to be used internally.
    explicit constexpr bitmask(underlying_type o) : mask_(o) {}

public:
    // Default ctor creates a bitmask with no options selected.
    constexpr bitmask() : mask_(0) {}

    // Creates a bitmask with just one bit set.
    // This ctor is intentionally non-explicit, to allow for stuff like:
    // FunctionExpectingBitmask(Options::Opt1)
    constexpr bitmask(option_type o) : mask_(mask_value(o)) {}

    // Set the bit corresponding to the given option.
    constexpr bitmask operator|(option_type t) {
        return bitmask(mask_ | mask_value(t));
    }

    // Get the value of the bit corresponding to the given option.
    constexpr bool operator&(option_type t) {
        return mask_ & mask_value(t);
    }

    constexpr bool operator&(const option_type t) const {
        return mask_ & mask_value(t);
    }

    constexpr void operator|=(option_type t) {
        mask_ |= mask_value(t);
    }

    constexpr bool operator==(option_type t) {
        return mask_ == mask_value(t);
    }

    private:
        underlying_type mask_ = 0;
};

// Creates a bitmask from two options, convenient for stuff like:
// FunctionExpectingBitmask(Options::Opt1 | Options::Opt2 | Options::Opt3)
// template <class option_type,
//           typename = typename std::enable_if<std::is_enum<option_type>::value>::type>
// constexpr bitmask<option_type> operator|(option_type lhs, option_type rhs) {
//     return bitmask<option_type>{lhs} | rhs;
// }
