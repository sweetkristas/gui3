#pragma once

//#include <fmt/ostream.h>
#include "spdlog/fmt/ostr.h"

namespace geometry
{
    template<typename T>
    struct point
    {
        T x;
        T y;
    };

    template<typename T>
    struct rect
    {
        T x1;
        T y1;
        T x2;
        T y2;
    };

    template<typename T>
    auto width(const rect<T>& r) {
        return r.x2 - r.x1;
    }

    template<typename T>
    auto height(const rect<T>& r) {
        return r.y2 - r.y1;
    }

    // template specialization for integer structure where the stored co-ordinates
    // are one less than the actual width.
    template<>
    inline auto width(const rect<int>& r) {
        return r.x2 - r.x1 + 1;
    }

    // template specialization for integer structure where the stored co-ordinates
    // are one less than the actual height.
    template<>
    inline auto height(const rect<int>& r) {
        return r.y2 - r.y1 + 1;
    }

    template<typename T>
    inline auto pt_in_rect(const point<T>& p, const rect<T>& r) {
        return p.x >= r.x1 && p.x <= r.x2 && p.y >= r.y1 && p.y <= r.y2;
    }

    template<typename T>
    inline auto pt_in_rect(int x, int y, const rect<T>& r) {
        return x >= r.x1 && x <= r.x2 && y >= r.y1 && y <= r.y2;
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const rect<T>& r) {
        return os << '[' << r.x1 << ' ' << r.y1 << ' ' << r.x2 << ' ' << r.y2 << ']';
    }    

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const point<T>& p) {
        return os << '(' << p.x << ' ' << p.y << ')';
    }

    template<typename T>
    rect<T> from_coords(const T x1, const T y1, const T x2, const T y2) {
        return rect<T>{ x1, y1, x2, y2 };
    }
}

typedef geometry::rect<int> rect_t;
typedef geometry::rect<float> rectf_t;

typedef geometry::point<int> point_t;
typedef geometry::point<float> pointf_t;
