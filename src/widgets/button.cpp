#include "button.hpp"
#include "context.hpp"

#include "nanovg.h"

#include "../color.hpp"

NVGcolor to_nvg_color(const color& c)
{
    NVGcolor res;
    res.r = c.r();
    res.g = c.g();
    res.b = c.b();
    res.a = c.a();
    return res;
}


namespace ui_nom
{
    button::button(std::string_view label)
        : label_(label)
    {
        set_policy(sizing_policy(policy::minimum_expanding, policy::fixed));

        init();
    }


    void button::init()
    {
        ///XXX move this to whenever label is changed, or font parameter is changed.
        context* ctx = context::get_context();
        NVGcontext* vg = ctx->get<NVGcontext*>();

        // Select font parameters
        nvgFontSize(vg, 24.f);//font_size_);
        nvgFontFace(vg, "sans");//font_name_);

        // calculate label bounds.
        float bounds[4];
        // x,y are dummy at the moment
        nvgTextBounds(vg, 0.0f, 0.0f, label_.c_str(), NULL, bounds);
        ///XXX move this to whenever label is changed, or font parameter is changed. -- ends

        int minw = static_cast<int>(bounds[2] - bounds[0]);
        int minh = static_cast<int>(bounds[3] - bounds[1]);
        set_content_size(minw, minh);
    }

    bool button::on_mouse_over(bitmask<key_modifiers> mods)
    {
        return false;
    }

    bool button::on_mouse_pressed(bitmask<key_modifiers> mods)
    {
        pressed_ = true;
        if(pressed_fn_) {
            pressed_fn_(this);
        }
        return false;
    }

    bool button::on_key_press(bitmask<key_modifiers> mods)
    {
        return false;
    }

    widget_ptr_t button::handle_clone() const
    {
        return std::make_unique<button>(*this);
    }

    void button::handle_draw(context* ctx) const 
    {

        // const float button_width = 120.f;
        // const float button_height = 30.f;
        // const float button_r = button_height / 6.0f;
        // const float stroke_width = 5.0f;
        auto [button_width, button_height] = get_content_size();
        const float button_r = button_height / 6.0f;
        const float stroke_width = 5.0f;

        
        static const NVGcolor stroke_color = to_nvg_color(color::color_gray());
        static const auto normal_color = to_nvg_color(color(200, 200, 200));
        static const auto mouse_over_color = to_nvg_color(color(180, 80, 80));
        static const NVGcolor mouse_down_color = to_nvg_color(color::color_blue());

        // const float x = 1000.f;
        // const float y = 220.f;
        const float x = static_cast<float>(get_x());
        const float y = static_cast<float>(get_y());

        const rect_t hit_area{ 
            static_cast<int>(x),
            static_cast<int>(y),
            static_cast<int>(x + button_width - 1),
            static_cast<int>(y + button_height - 1)
         };

        NVGcontext* vg = ctx->get<NVGcontext*>();
		nvgBeginPath(vg);
		nvgRoundedRect(vg, x, y, button_width, button_height, button_r);
		nvgStrokeColor(vg, stroke_color);
		nvgStrokeWidth(vg, stroke_width);
		nvgStroke(vg);

        const auto [mx, my] = ctx->get_mouse_pos();

        if(pt_in_rect(mx, my, hit_area)) {
		 	if(ctx->get_mouse_state() & mouse_state::ANY) {
		 		nvgFillColor(vg, mouse_down_color);
		 	} else {
		 		nvgFillColor(vg, mouse_over_color);
		 	}
		 } else {
			nvgFillColor(vg, normal_color);
		}
		nvgFill(vg);
        
        NVGpaint shadowPaint;
        shadowPaint = nvgBoxGradient(vg, x, y+5, button_width, button_height, button_r * 2, 10, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
        nvgBeginPath(vg);
        nvgRect(vg, x-10, y-10, button_width+30, button_height+40);
        nvgRoundedRect(vg, x, y, button_width, button_height, button_r);
        nvgPathWinding(vg, NVG_HOLE);
        nvgFillPaint(vg, shadowPaint);
        nvgFill(vg);
    }
}
