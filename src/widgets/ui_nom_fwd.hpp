#pragma once

#include <memory>
#include <vector>

#include "bitmask.hpp"

namespace ui_nom
{
    class layout;
    typedef std::unique_ptr<layout> layout_ptr_t;

    class widget;
    typedef std::unique_ptr<widget> widget_ptr_t;

    class context;

    enum class key_modifiers
    {
        LSHIFT      = (1 <<  0),
        RSHIFT      = (1 <<  1),
        LCTRL       = (1 <<  2),
        RCTRL       = (1 <<  3),
        LALT        = (1 <<  4),
        RALT        = (1 <<  5),
        LGUI        = (1 <<  6),
        RGUI        = (1 <<  7),
        NUM         = (1 <<  8),
        CAPS        = (1 <<  9),
        MODE        = (1 << 10),

        SHIFT       = LSHIFT | RSHIFT,
        CTRL        = LCTRL | RCTRL,
        ALT         = LALT | RALT,
        GUI         = LGUI | RGUI,
    };

    enum class mouse_state
    {
        LEFT        = (1 << 0),
        MIDDLE      = (1 << 1),
        RIGHT       = (1 << 2),
        X1          = (1 << 3),
        X2          = (1 << 4),

        ANY         = std::numeric_limits<int>::max(),
    };

    enum class alignment
    {
        LEFT        = (1 << 0),
        RIGHT       = (1 << 1),
        H_CENTER    = (1 << 2),
        TOP         = (1 << 9),
        BOTTOM      = (1 << 10),
        V_CENTER    = (1 << 11),
        BASELINE    = (1 << 12),

        CENTER      = H_CENTER | V_CENTER,
        H_MASK      = ~(LEFT | RIGHT | H_CENTER),
        V_MASK      = ~(TOP | BOTTOM | V_CENTER | BASELINE),
    };
}
