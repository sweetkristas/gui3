#pragma once

#include "widget.hpp"
#include <string>
#include <string_view>

namespace ui_nom
{
    class button;
    typedef std::function<void(button*)> button_callback_fn_t;

    class button : public widget
    {
    public:
        explicit button(std::string_view label);

        bool is_pressed() const { return pressed_; }
        void on_pressed(button_callback_fn_t fn) { pressed_fn_ =  fn; }

        void set_label(std::string_view label) { label_ = label; /* trigger draw update? */}
        void init();
    private:
        button() = delete;
        bool on_mouse_over(bitmask<key_modifiers> mods) override;
        bool on_mouse_pressed(bitmask<key_modifiers> mods) override;
        bool on_key_press(bitmask<key_modifiers> mods) override;
        widget_ptr_t handle_clone() const override;
        void handle_draw(context* ctx) const override;
        
        std::string label_{};
        bool pressed_ = false;
        button_callback_fn_t pressed_fn_{};
    };
}