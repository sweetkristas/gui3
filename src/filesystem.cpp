#include <filesystem>
#include <fstream>
#include <sstream>
#include "asserts.hpp"
#include "filesystem.hpp"

namespace sys
{
    namespace fs = std::filesystem;

    std::string read_file(const std::string& filename)
    {
        std::ifstream ifs(filename);
        std::stringstream ss;
        ss << ifs.rdbuf();
        return ss.str();        
    }

    std::vector<char> read_file_to_vector(const std::string& filename)
    {
        std::ifstream ifs(filename);
        return std::vector<char>(std::istreambuf_iterator<char>(ifs), std::istreambuf_iterator<char>());
    }

    file_map get_unique_files(const std::string& folder)
    {
        file_map res;
        fs::path p(folder);
        ASSERT_LOG(p.has_parent_path(), "No parent path found for given folder: {}", folder);
        for(auto& p : fs::recursive_directory_iterator(p.parent_path())) {
            if(fs::is_regular_file(p)) {
                ASSERT_LOG(p.path().has_filename(), "No filename, though file marked as regular: {}", p.path().generic_string());
                auto fn = p.path().filename().generic_string();

                auto it = res.find(fn);
                if(it == res.end()) {
                    res[fn] = p.path().generic_string();
                } else {
                    LOG_WARN("Skipping adding file: {} from {} which has a would over-write existing item {}", fn, p.path().generic_string(), it->second);
                }
            }
        }
        return res;
    }
}
