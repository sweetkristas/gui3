#pragma once

#include <map>
#include <string>
#include <vector>

namespace sys
{
    typedef std::map<std::string, std::string> file_map;

    std::string read_file(const std::string& filename);
    std::vector<char> read_file_to_vector(const std::string& filename);

    // Returns a list of all the files with unique names that exist in the
    // given folder and its sub-folders.
    file_map get_unique_files(const std::string& folder);
}
