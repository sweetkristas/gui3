#pragma once

#include <cstdint>
#include <string>

inline uint32_t constexpr string_hash(const char* input)
{
	// Based on djb2 by Dan Bernstein from http://www.cse.yorku.ca/~oz/hash.html
	return *input ? static_cast<uint32_t>(*input) ^ (33 * string_hash(input + 1)) : 5381;
}
