#include "SDL2/SDL.h"
#include "GL/gl3w.h"
#include "rapidxml.hpp"

#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

#include "asserts.hpp"

#include "widgets/button.hpp"
#include "widgets/layout.hpp"
#include "widgets/context.hpp"

// void render_pattern(NVGcontext* vg, NVGLUframebuffer* fb, float t, float pxRatio)
// {
// 	int winWidth, winHeight;
// 	int fboWidth, fboHeight;
// 	int pw, ph, x, y;
// 	float s = 20.0f;
// 	float sr = (cosf(t)+1)*0.5f;
// 	float r = s * 0.6f * (0.2f + 0.8f * sr);

// 	if (fb == nullptr) return;

// 	nvgImageSize(vg, fb->image, &fboWidth, &fboHeight);
// 	winWidth = (int)(fboWidth / pxRatio);
// 	winHeight = (int)(fboHeight / pxRatio);

// 	// Draw some stuff to an FBO as a test
// 	nvgluBindFramebuffer(fb);
// 	glViewport(0, 0, fboWidth, fboHeight);
// 	glClearColor(0, 0, 0, 0);
// 	glClear(GL_COLOR_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
// 	nvgBeginFrame(vg, winWidth, winHeight, pxRatio);

// 	pw = (int)ceilf(winWidth / s);
// 	ph = (int)ceilf(winHeight / s);

// 	nvgBeginPath(vg);
// 	for (y = 0; y < ph; y++) {
// 		for (x = 0; x < pw; x++) {
// 			float cx = (x+0.5f) * s;
// 			float cy = (y+0.5f) * s;
// 			nvgCircle(vg, cx,cy, r);
// 		}
// 	}
// 	nvgFillColor(vg, nvgRGBA(220,160,0,200));
// 	nvgFill(vg);

// 	nvgEndFrame(vg);
// 	nvgluBindFramebuffer(nullptr);
// }


// void draw_box_shadow(NVGcontext* vg, float x, float y, float w, float h, float cornerRadius)
// {
// 	NVGpaint shadowPaint;
// 	shadowPaint = nvgBoxGradient(vg, x, y+5, w, h, cornerRadius * 2, 10, nvgRGBA(0,0,0,128), nvgRGBA(0,0,0,0));
// 	nvgBeginPath(vg);
// 	nvgRect(vg, x-10,y-10, w+30,h+40);
// 	nvgRoundedRect(vg, x,y, w,h, cornerRadius);
// 	nvgPathWinding(vg, NVG_HOLE);
// 	nvgFillPaint(vg, shadowPaint);
// 	nvgFill(vg);
// }

class graphics 
{
public:
	explicit graphics(int w, int h)
		: win_width_(w)
		, win_height_(h) 
	{
		if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_GAMECONTROLLER) != 0) {
			LOG_CRITICAL("Unable to initialize SDL: {}", SDL_GetError());
		}
		// Setup window
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

		SDL_DisplayMode current;
		SDL_GetCurrentDisplayMode(0, &current);
		window_ = SDL_CreateWindow("gui3 SDL2+OpenGL3+NanoVG example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
		gl_context_ = SDL_GL_CreateContext(window_);
		SDL_GL_SetSwapInterval(1); // Enable vsync
		//SDL_GL_SetSwapInterval(0);
		gl3wInit();

		glEnable(GL_MULTISAMPLE);

		SDL_GetWindowSize(window_, &win_width_, &win_height_);
		SDL_GL_GetDrawableSize(window_, &drawable_width_, &drawable_height_);
		LOG_INFO("Window Size: {}x{}; Drawable Size: {}x{}", win_width_, win_height_, drawable_width_, drawable_height_);
	}
	~graphics() {
		SDL_GL_DeleteContext(gl_context_);
		SDL_DestroyWindow(window_);
		SDL_Quit();
	}
	void swap() {
        SDL_GL_SwapWindow(window_);
	}
	std::pair<int,int> get_window_size() {
		SDL_GetWindowSize(window_, &win_width_, &win_height_);
		return std::make_pair(win_width_, win_height_);
	}
	std::pair<int, int> get_drawable_size() {
		SDL_GL_GetDrawableSize(window_, &drawable_width_, &drawable_height_);
		return std::make_pair(drawable_width_, drawable_height_);
	}
	void clear() {
        glViewport(0, 0, win_width_, win_height_);
        glClearColor(clear_color_[0], clear_color_[1], clear_color_[2], clear_color_[3]);
        glClear(GL_COLOR_BUFFER_BIT);
	}
	ui_nom::context* get_drawing_context() {
		return ui_nom::context::get_context();
	}
private:
	int win_width_ = 0;
	int win_height_ = 0;
	int drawable_width_ = 0;
	int drawable_height_ = 0;
	SDL_Window* window_ = nullptr;
	SDL_GLContext gl_context_{};
    float clear_color_[4] = { 0.45f, 0.55f, 0.60f, 1.00f };
	std::unique_ptr<ui_nom::context> ctx_;
};

class ui_tester 
{
public:
	ui_tester() 
	{
		using namespace ui_nom;
		test_ = widget::create<dialog>();

		auto hlayout = layout::create<horiz_layout>();
		b1_ = hlayout->add_widget<button>("Push Me!");
		b1_->on_pressed([](button*){
			LOG_INFO("Button b1 was pressed (callback).");
		});

		test_->set_layout(std::move(hlayout));

		//process
		//draw
	}
	void process(double dt, int width, int height)
	{
		test_->process(dt, width, height);

		if(b1_->is_pressed()) {
		 	LOG_INFO("Button b1 pressed (direct).");
		}
		
		// if((*b2)()) {
		// 	spdlog::get("console")->info("Button b2 pressed.");
		// }
	}
	void draw(ui_nom::context* ctx)
	{
		test_->draw(ctx);
	}
private:
	std::unique_ptr<ui_nom::dialog> test_;
	ui_nom::button* b1_;
};

class texture 
{
	public:
	private:
		GLuint id_;
};

int main(int argc, char* argv[])
{
    auto console = spdlog::stdout_color_mt("console");

	std::vector<std::string> args;
	for(int n = 1; n != argc; ++n) {
		args.emplace_back(argv[n]);
	}
	std::filesystem::path program(argv[0]);

	//std::cout << program.filename().generic_string() << "\n";

	graphics g(1280, 720);

	auto drawing_ctx = g.get_drawing_context();

	// The image pattern is tiled, set repeat on x and y.
	// fb = nvgluCreateFramebuffer(drawing_ctx.get<NVGcontext*>(), (int)(100*px_ratio), (int)(100*px_ratio), NVG_IMAGE_REPEATX | NVG_IMAGE_REPEATY /*| NVG_IMAGE_MULTISAMP*/);
	// if (fb == NULL) {
	// 	console->error("Could not create FBO.\n");
	// 	return -1;
	// }

	int mx, my;

	ui_tester ui_test; 

    bool done = false;
	double start_time = SDL_GetPerformanceCounter();
	double acc_t = 0;
    while (!done) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
		    //ImGui_ImplSdlGL3_ProcessEvent(&event);
            if (event.type == SDL_QUIT) {
                done = true;
            } else if(event.type == SDL_CONTROLLERDEVICEADDED) {
                console->debug("Controller device added, index: {}", event.cdevice.which);
            } else if(event.type == SDL_CONTROLLERDEVICEREMOVED) {
                console->debug("Controller device removed, index: {}", event.cdevice.which);
            } else if(event.type == SDL_CONTROLLERDEVICEREMAPPED ) {
                console->debug("Controller device re-mapped, index: {}", event.cdevice.which);
            } else if(event.type == SDL_CONTROLLERAXISMOTION) {
                console->debug("Controller axis motion, index: {}, axis: {}, value: {}", event.caxis.which, event.caxis.axis, event.caxis.value);
            } else if(event.type == SDL_CONTROLLERBUTTONDOWN) {
                console->debug("Controller button down, index: {}, button: {}, state: {}", event.cbutton.which, event.cbutton.button, event.cbutton.state == SDL_PRESSED ? "Pressed" : "Released");
            }
        }

		double new_time = SDL_GetPerformanceCounter();
		const double dt = (new_time - start_time) / SDL_GetPerformanceFrequency();
		start_time = new_time;
		acc_t += dt;

		auto [wwidth, wheight] = g.get_window_size();
		auto [dwidth, dheight] = g.get_drawable_size();
		float px_ratio = dwidth / wwidth;

		g.clear();

		drawing_ctx->begin_frame(wwidth, wheight, px_ratio);

		// if(fb != nullptr) {
		// 	NVGpaint img = nvgImagePattern(vg, 0, 0, 100, 100, 0, fb->image, 1.0f);
		// 	nvgSave(vg);

		// 	for (int i = 0; i < 20; i++) {
		// 		nvgBeginPath(vg);
		// 		nvgRect(vg, 10 + i*30,10, 10, win_height-20);
		// 		nvgFillColor(vg, nvgHSLA(i/19.0f, 0.5f, 0.5f, 255));
		// 		nvgFill(vg);
		// 	}

		// 	nvgBeginPath(vg);
		// 	nvgRoundedRect(vg, 140 + sinf(acc_t*1.3f)*100, 140 + cosf(acc_t*1.71244f)*100, 250, 250, 20);
		// 	nvgFillPaint(vg, img);
		// 	nvgFill(vg);
		// 	nvgStrokeColor(vg, nvgRGBA(220,160,0,255));
		// 	nvgStrokeWidth(vg, 3.0f);
		// 	nvgStroke(vg);

		// 	nvgRestore(vg);
		// }

		ui_test.process(dt, wwidth, wheight);
		ui_test.draw(drawing_ctx);

		drawing_ctx->end_frame(dt);

		g.swap();
    }

    return 0;
}
